<?php
/***
**** @class configuration file for class: protect (in adminpro_class.php)
**** @project: AdminPro Class
**** @version: 1.3;
**** @author: Giorgos Tsiledakis;
**** @date: 2004-09-04;
**** @license: GNU GENERAL PUBLIC LICENSE;
**** PLEASE CONFIGURE THIS FILE FIRST
***/

/*
**MySQL CONFIGURATION*************************************************************
*/
$globalConfig['dbhost']=DB_SERVER; // Your MySQL Server Host URL
$globalConfig['dbuser']=DB_SERVER_USERNAME; // Your MySQL Username
$globalConfig['dbpass']=DB_SERVER_PASSWORD; // Your MySQL Password
$globalConfig['dbase']=DB_DATABASE; // Your MySQL Database name
$globalConfig['tbl']="usuarios"; // The name of the MySQL table to store the data required
$globalConfig['tblPermissions']="permissoes"; // The name of the MySQL table to check the permissions data required
$globalConfig['tblGroup']="grupos"; // The name of the MySQL table to check the permissions data required
$globalConfig['tblModules']="modulos"; // The name of the MySQL table to check the modules data required
$globalConfig['tblID']="id"; // The name of the ID field of the MySQL table
$globalConfig['tblUserName']="login"; // The name of the Username field of the MySQL table
$globalConfig['tblUserPass']="senha"; // The name of the Userpassword field of the MySQL table
##
$globalConfig['tblIsAdmin']="is_admin"; // The name of the Administator field of the MySQL table
$globalConfig['tblUserGroup']="grupo_id"; // The name of the User Group field of the MySQL table
$globalConfig['tblHomePage']="home_page"; // The name of the User Group field of the MySQL table
$globalConfig['tblSessionID']="session_id"; // The name of the ID field of the MySQL table
$globalConfig['tblLastLog']="last_log"; // The name of the Time field of the MySQL table
$globalConfig['tblUserRemark']="obs"; // The name of the Remarks field of the MySQL table
/*
**END MySQL CONFIGURATION*********************************************************
*/
/*
**GENERAL CONFIGURATION***********************************************************
*/
/*
$globalConfig['acceptNoCookies']
true = display an error message if the user has deactivated cookies
false = no error message; you should though pass somehow (POST/GET) the session ID on each link!!
e.g. your_next_page.php?PHPSESSID=".session_id(); etc.
**********************************************************************************
*/
$globalConfig['acceptNoCookies']=true;
/*
**********************************************************************************
*/
$globalConfig['inactiveMin']="1440"; // The time in minutes to force new login, if account has been inactive
$globalConfig['loginUrl']="../admin/login2.php"; // The URL of the login page
$globalConfig['logoutUrl']="../admin/login2.php"; // The URL of the logout page
/*
**END GENERAL CONFIGURATION*******************************************************
*/
/*
**REMEMBER LOGIN CONFIGURATION****************************************************
*/
$globalConfig['enblRemember']=false; // set true to enable Remember Me function
$globalConfig['cookieRemName']="AdminPro-RememberMyName"; // name of username cookie
$globalConfig['cookieRemPass']="AdminPro-RememberMyPass"; // name of password
$globalConfig['cookieExpDays']="30"; // num of days, when remember me cookies expire
/*
**END REMEMBER LOGIN CONFIGURATION************************************************
*/
/*
**HASH CONFIGURATION**************************************************************
$globalConfig['isMd5']
1 = passwords will be stored md5 encrypted on database
other number = passwords will be stored as is on database
**********************************************************************************
*/
$globalConfig['isMd5']="1";
/*
**END HASH CONFIGURATION**********************************************************
*/
/*
**ERROR PAGE CONFIGURATION********************************************************
*/
/*
$globalConfig['errorCssUrl']
the url of the external stylesheet file for the error pages
please leave it blank: $globalConfig['errorCssUrl']=""; if you do not want to use one
**********************************************************************************
*/
$globalConfig['errorCssUrl']="adminpro.css";
/*
**********************************************************************************
*/
/*
$globalConfig['errorCharset']
the Charset for the error pages, default: iso-8859-1
please leave it blank: $globalConfig['errorCharset']=""; if you do not want to use one
**********************************************************************************
*/
$globalConfig['errorCharset']="iso-8859-1";
/*
**********************************************************************************
*/
$globalConfig['errorPageTitle']="N�o autorizado!";
$globalConfig['errorPageH1']="N�o autorizado!";
$globalConfig['errorPageLink']="clique para logar";
$globalConfig['errorNoCookies']="Voc� precisa aceitar os cookies para proceder!";
$globalConfig['errorNoLogin']="Por favor logue primeiro para visualizar essa p�gina!";
$globalConfig['errorInvalid']="Usu�rio ou senha inv�lidos!";
$globalConfig['errorDelay']="Sua conta estava inativa inativa por muito tempo<br>";
$globalConfig['errorDelay'].="Ou voc� usou o login mais de uma vez!<br>";
$globalConfig['errorDelay'].="Essa sess�o n�o est� mais ativa!!";
$globalConfig['errorNoAdmin']="Voc� precisa ter direitos administrativos para visualizar essa p�gina!";
$globalConfig['errorNoGroup']="Voc� n�o pertence ao grupo de usu�rio necess�rio para visualizar essa p�gina!";
/*							
**END ERROR PAGE CONFIGURATION****************************************************
*/
?>