<? include("topo.php"); 
////////////////////////////////////////
include("../includes/functions.php");
include("../fckeditor/fckeditor.php");

conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
    die();
}
if($permissao){
	
	$altera_status = $_GET['altera_status'];
	//altera o status da novidade no site
	if($altera_status == 1)
	{
		$id_novidade = $_GET['id_novidade'];
		
		$query_status 	= "select status from novidades where id = $id_novidade";
		$result_status	= mysql_query($query_status);
		$status 		= mysql_result($result_status, 0, 'status');
		
		if($status == 0)
		{ 
			$query_update	= "update novidades set status = 1 where id = $id_novidade";
			$msg_status		= "Novidade ativada na no site.";
		}
		else
		{
			$query_update	= "update novidades set status = 0 , homeDestaque = 0, homeNovidade = 0 where id = $id_novidade";
			$msg_status		= "Novidade desativada no site.";
		}
		$result_update	= mysql_query($query_update);
		
		if($result_update)
		{
			alert($msg_status);
			redirect("gerencia_novidades.php");
			die();
		}
		
	}
	//altera o status do destaque no box da home
	if($altera_status == 2)
	{
		$id_novidade = $_GET['id_novidade'];		
		$query_status 	= "select status,homeDestaque from novidades where id = $id_novidade";
		$result_status	= mysql_query($query_status);
		$status			= mysql_result($result_status, 0, 'status');
		
		if($status == 0)
		{
			alert('Para ativar este destaque no box da home, o status da novidade deve estar como ativo no site.');
			redirect("gerencia_novidades.php");
			die();
		}
		$homeDestaque 	= mysql_result($result_status, 0, 'homeDestaque');
				
		if($homeDestaque == 0)
		{ 
			//$query_zerar_destaque	= "update novidades set homeDestaque = 0";
			//$result_zerar_destaque	= mysql_query($query_zerar_destaque);
			$query_update	= "update novidades set homeDestaque = 1 where id = $id_novidade";
			$msg_status		= "Destaque ativado no box da Home.";
		}
		else
		{
			$query_update	= "update novidades set homeDestaque = 0 where id = $id_novidade";
			$msg_status		= "Destaque desativado no box da Home.";
		}
		$result_update	= mysql_query($query_update);
		
		if($result_update)
		{
			alert($msg_status);
			redirect("gerencia_novidades.php");
			die();
		}
	}
	//altera o status da novidade no box da home
	if($altera_status == 3)
	{
		$id_novidade = $_GET['id_novidade'];		
		$query_status 	= "select status,homeNovidade from novidades where id = $id_novidade";
		$result_status	= mysql_query($query_status);
		$status			= mysql_result($result_status, 0, 'status');
		
		if($status == 0)
		{
			alert('Para ativar esta novidade no box da home, o status da novidade deve estar como ativo no site.');
			redirect("gerencia_novidades.php");
			die();
		}
		$homeNovidade 	= mysql_result($result_status, 0, 'homeNovidade');
				
		if($homeNovidade == 0)
		{ 
			//$query_zerar_novidade	= "update novidades set homeNovidade = 0";
			//$result_zerar_novidade	= mysql_query($query_zerar_novidade);
			$query_update	= "update novidades set homeNovidade = 1 where id = $id_novidade";
			$msg_status		= "Novidade ativada no box da Home.";
		}
		else
		{
			$query_update	= "update novidades set homeNovidade = 0 where id = $id_novidade";
			$msg_status		= "Novidade desativada no box da Home.";
		}
		$result_update	= mysql_query($query_update);
		
		if($result_update)
		{
			alert($msg_status);
			redirect("gerencia_novidades.php");
			die();
		}
		
		
	}
	///////// Preparando para paginacao \\\\\\\\\\\
	$order = "nome desc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros para paginacao
	$query = "SELECT count(id) as total from novidades";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from novidades order by $order limit $inicio,$limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_novidades.html');
	
	while ($row = mysql_fetch_array($result)) 
	{
		$id_novidade	= $row['id'];
		$nome			= $row['nome'];
		$titulo			= $row['titulo'];
		$descricao 		= $row['descricao'];
		$conteudo 		= html_entity_decode($row['conteudo']);
		$data_inicio	= saidaData($row['inicio']);
		$data_fim		= saidaData($row['fim']);
		$hora_inicio	= saidaHora($row['inicio']);
		$hora_fim		= saidaHora($row['fim']);
		
		$tamanho_max	= 80;		
		$abrev =  substr(strip_tags($conteudo) , 0 , $tamanho_max); 
		if(strlen(strip_tags($conteudo)) > $tamanho_max) $abrev = $abrev. "...";
		else $abrev = $conteudo;
		
		//libera o bloqueia a exibicao no site
		if($row['status'] == 1){
			$status = "<img src='../imagens_layout/tick.png ' border=0 alt='Novidade ativa na site' />";
			$title 	= "Novidade ativa na site";
		}else{
			$status = "<img src='../imagens_layout/slash.png ' border=0 alt='Novidade inativa na site' />";
			$title 	= "Novidade inativa na site";
		}
		
		//libera o bloqueia a exibicao no box da home
		if($row['homeDestaque'] == 1){
			$homeDestaque = "<img src='../imagens_layout/tick.png ' border=0 alt='Destaque ativo no box da home' />";
			$titleDestaque 	= "Destaque ativo no box da home";
		}else{
			$homeDestaque = "<img src='../imagens_layout/slash.png ' border=0 alt='Destaque inativo no box da home' />";
			$titleDestaque 	= "Destaque inativo no box da home";
		}
		
		//libera o bloqueia a exibicao no box da home
		if($row['homeNovidade'] == 1){
			$homeNovidade = "<img src='../imagens_layout/tick.png ' border=0 alt='Novidade ativa no box da home' />";
			$titleNovidade 	= "Novidade ativa no box da home";
		}else{
			$homeNovidade = "<img src='../imagens_layout/slash.png ' border=0 alt='Novidade inativa no box da home' />";
			$titleNovidade 	= "Novidade inativa no box da home";
		}
		
		$acao_editar	= "<a href='edita_novidade.php?id_novidade=$id_novidade'><img src='../imagens_layout/btn_editar_p.jpg' border=0 /></a>";
		
		$acao_excluir	= "<a onclick=\"confirma('Deseja excluir esta a novidade ".strip_tags($nome)."?', 'exclui_novidade.php?id_novidade=$id_novidade');\" href='#'><img src='../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
			
		$campos 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt'>$nome</td>
								<td class='td_txt'>$titulo</td>
								<td class='td_txt'>$descricao</td>
								
								<td class='td_txt'>$data_inicio-$hora_inicio</td>
								<td class='td_txt'>$data_fim-$hora_fim</td>
								<td class='td_txt' align='center'>
									<a href='gerencia_novidades.php?altera_status=1&id_novidade=$id_novidade' border=0 title='$title'  >$status</a>
								</td>".
								/*<td class='td_txt' align='center'>
									<a href='gerencia_novidades.php?altera_status=2&id_novidade=$id_novidade' border=0 title='$titleDestaque'  >$homeDestaque</a>
								</td>*/
								"<td class='td_txt' align='center'>
									<a href='gerencia_novidades.php?altera_status=3&id_novidade=$id_novidade' border=0 title='$titleNovidade'>$homeNovidade</a>
								</td>
								<td class='td_txt' align=center nowrap >
									$acao_editar &nbsp; $acao_excluir
								</td>
							</tr> ";
		
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	///////////// paginacao \\\\\\\\\\\\\\\\\\
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_novidades.php?order=nome asc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_novidades.php?order=nome asc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_novidades.php?order=nome asc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
	
		////////////////////////////////////////////////////////////////
		
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;"><?  echo $paginacao; ?></div>
        </div>
    <?
}
else
{
	alert("Permissao Negada");
	redirect("index.php");
	die();
}
	include("rodape.php");
	////////////////////////////////////////////////////////////////
?>
