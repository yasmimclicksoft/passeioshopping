<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_fraldario", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$submit 	= $_POST['submit'];
	$nome		= $_POST['nome'];
	$cpf		= $_POST['cpf'];
	$email		= $_POST['email'];
	$id_cliente = $_POST['id_cliente']; 
	$telefone	= $_POST['telefone'];
	$cep		= $_POST['cep'];
	$endereco	= $_POST['endereco'];
	$bairro		= $_POST['bairro'];
	$cidade		= $_POST['cidade'];
	$estado_id	= $_POST['estado'];
	$dia_1		= $_POST['dia_1'];
	$dia_2		= $_POST['dia_2'];
	
	if(isset($submit))
	{
			
		$query_insert = "insert into responsavel(nome,cpf,telefone,email,cep,endereco,bairro,cidade,estado_id,dia_1,dia_2) values('$nome','$cpf','$telefone','$email','$cep','$endereco','$bairro','$cidade','$estado_id','$dia_1','$dia_2')";
		$result_insert = mysql_query($query_insert); 
		
		$id_responsavel = mysql_insert_id();
	
		$p_contador = $_POST['contador'];	
		
		$p_contador++;
		
		for ($i=1;$i<$p_contador;$i++)
		{
			$campo1 = "nome__".$i;
			$campo2 = "nascimento__".$i;
			$campo3 = "sexo__".$i;
			$value1 = $_POST[$campo1];
			$value2 = $_POST[$campo2];
			$value3 = $_POST[$campo3];
			
			if($_POST[$campo2] != "")
				$value2 = entradaData($_POST[$campo2]);
			
			
			if ($value1 == "" and $value2 == ""){
			}
			else
			{
				$query_horas = "INSERT INTO crianca(nome,nascimento,sexo,responsavel_id) VALUES ('".$value1."', '".$value2."','".$value3."','$id_responsavel')";
				$result_horas = mysql_query($query_horas);
			}
		}		
				
		if($result_insert)
		{
			historicoUsuario("Cadastro de responsável");
			alert('Responsável cadastrado com sucesso.');	
			redirect("gerencia_responsavel.php");	
		}
		else
		{
			alert('Erro ao cadastrar responsável.');	
			redirect("cadastro_responsavel.php");
		}
		
	}
	else
	{
		
		
		$conteudo_tpl = AbrePag(DIR_TEMPLATES.'cadastro_responsavel.html');
		$conteudo_tpl = str_replace ('##ACTION##', 'cadastro_responsavel.php', $conteudo_tpl);
		
		$submit_id 	= "<input name='id_cliente' type= 'hidden' value ='".$id_cli."' />";
		$conteudo_tpl	= str_replace ("##HIDDEN##" , $submit_id , $conteudo_tpl);
		
	# cep vindo por web service
	$cep = $_GET['cep'];
	###########################
	
	if(!isset($cep)){
		$FazCombo_estados 	= FazCombo(estado, nome, estados, '0', 'label');
		$conteudo_tpl 		= str_replace("##ESTADOS##", $FazCombo_estados ,$conteudo_tpl);
	}

	# tratando a busca do cep por web service
	if(isset($cep))
	{
		#buscando cep
		$resultado = @file_get_contents('http://republicavirtual.com.br/web_cep.php?cep='.urlencode($cep).'&formato=query_string');   
		if(!$resultado){   
			$resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";   
		}   
    	parse_str($resultado, $retorno); 
		
		$resultado_busca = $retorno;
		
		#se a resposta foi positiva
		if($resultado_busca['resultado'] == 1)
		{
			//$conteudo_tpl = str_replace ("##DISPLAY##", "style='display:none'" , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##NOME##", $_GET['nome'] , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##CPF##", $_GET['cpf'] , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##CEP##", $cep , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##ENDERECO##", $resultado_busca['tipo_logradouro']." ".$resultado_busca['logradouro'] , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##BAIRRO##", $resultado_busca['bairro'] , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##CIDADE##", $resultado_busca['cidade'] , $conteudo_tpl);
			
			#buscando o id do estado retornado
			$queryEstado = "select id from estados where sigla = '".$resultado_busca['uf']."' ";
			$resultEstado= mysql_query($queryEstado);
			$numEstado = mysql_num_fields($resultEstado);
			
			if($numEstado > 0)	$estado_id = mysql_result($resultEstado,0,'id');
			
			#montado o droplist com o estado recebido
			$FazCombo_estados 	= FazCombo(estado, nome, estados, $estado_id , 'label');
			$conteudo_tpl 		= str_replace("##ESTADOS##", $FazCombo_estados ,$conteudo_tpl);
		}
		else
		{
			$conteudo_tpl = str_replace ("##NOME##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace ("##CPF##", '' , $conteudo_tpl);
			$conteudo_tpl 	= str_replace ("##ENDERECO##", '' , $conteudo_tpl);
			$conteudo_tpl 	= str_replace ("##BAIRRO##", '' , $conteudo_tpl);
			$conteudo_tpl 	= str_replace ("##CIDADE##", '' , $conteudo_tpl);
			$conteudo_tpl 	= str_replace ("##CEP##", '' , $conteudo_tpl);
			
			#monta o droplist normal sem o estado selecionado
			$FazCombo_estados 	= FazCombo(estado, nome, estados, '0', 'label');
			$conteudo_tpl 		= str_replace("##ESTADOS##", $FazCombo_estados ,$conteudo_tpl);
			
		
		}
	}
	else
	{
		$conteudo_tpl = str_replace ("##NOME##", '' , $conteudo_tpl);
		$conteudo_tpl = str_replace ("##CPF##", '' , $conteudo_tpl);
		$conteudo_tpl 	= str_replace ("##ENDERECO##", '' , $conteudo_tpl);
		$conteudo_tpl 	= str_replace ("##BAIRRO##", '' , $conteudo_tpl);
		$conteudo_tpl 	= str_replace ("##CIDADE##", '' , $conteudo_tpl);
		$conteudo_tpl 	= str_replace ("##CEP##", '' , $conteudo_tpl);
	}
		
		$criancas 		= "
						<br/>Nome:<input type='text' name='nome__1' size='40' maxlength='50' class='label'> 
						Nascimento:<input type='text' class='label' name='nascimento__1' size='10' maxlength='10' onKeyPress='return digitos(event, this);' onKeyUp=\"Mascara('DATA',this,event);\"> 
						Sexo:
						Masculino<input type='radio' id='sexom' name='sexo__1' value='m' size='40' maxlength='50'>
						Feminino<input type='radio' id='sexof' name='sexo__1' value='f' size='40' maxlength='50'>
						*<input type='hidden' name='contador' value='1' id=\"theValue\" />
						<br /><div id='minhaDiv'></div><br />
						<a class=\"txt_td\" href=\"javascript:;\" onclick=\"addChild();\">Adicionar Criança</a>
						<br /><br />* Preenchimento obrigatorio";
						
		$conteudo_tpl	= str_replace ("##CRIANCA##" , $criancas , $conteudo_tpl);						
			
	////////////////////////////////////////////////////////////////
		
		include("../navegacao.php");
	?>
		<div class="conteudo">
	<?
		echo $conteudo_tpl;
	?>
		</div> 
	<?
	
		include("../rodape.php");
		////////////////////////////////////////////////////////////////
	}
}
?>