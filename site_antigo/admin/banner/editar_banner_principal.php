<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_cadastrar_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	$submit = $_POST['submit'];
	
	if(isset($submit))
	{
		$id_banner 		= $_POST['id_banner'];
		$arquivo		= $_FILES['arquivo'];
		$nome			= $_POST['nome'];
		$ordem			= $_POST['ordem'];
		
		$data_entrada 	= entradaData($_POST['dataEntrada']);
		$data_saida		= entradaData($_POST['dataSaida']);
		
		$query_select 	= "select * from banner_principal where id = $id_banner";
		$result_select 	= mysql_query($query_select);
		$row			= mysql_fetch_array($result_select);
		$flash 			= $row['flash'];
		
		$data 			=  explode(" ",dataAtual());
		$data_atual 	= date('Ymd');
		
		///retirando os "-" da data para depois ser feita a comparação////////////////////////////////////////////////////////////
		$data_fim_numeral = str_replace("-", "",  $data_saida);
		//verificando se a data de saída é menor ou igual a data atual, se for o status passa a ser desativado
		if($data_fim_numeral <= $data_atual)
			$desativa_status = " , status = 0";
		else
			$desativa_status = "";
		
		if($arquivo['size'] != 0 or $arquivo['size'] != "")
		{
			//verificando se o arquivo importado � um swf, se n�o o usu�rio � redirecionado para p�gina de listagem sem cadastrar o arquivo
			if($_FILES['arquivo']['type'] != "application/x-shockwave-flash")
			{
				alert("Apenas arquivos com extensão swf são aceitas");
				redirect("gerencia_banner_principal.php");
			}
			else
			{
				unlink("../../swf/".$flash);
				if($_FILES['arquivo']['name'] != "")
				{
					$nome_trailler 	= basename($_FILES['arquivo']['name']);
					$ext 			= explode('.', $nome_trailler);
					$extensao		= strtolower($ext[1]);
					
					$path 		= "../../swf/".$id_banner."_".$ext[0].".".$ext[1];
					
					$arquivo	= $id_banner."_".$ext[0].".".$ext[1];
					if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $path )) 
					{
						$query2 ="update banner_principal set nome = '$nome', inicio = '$data_entrada', fim = '$data_saida' ,flash = '$arquivo', ordem = '$ordem' $desativa_status where id = $id_banner ";
						$result2 = mysql_query($query2);
						
					}
					if($result2)
					{
						alert('Banner principal alterado com sucesso.');
						redirect("gerencia_banner_principal.php");	
					}
					else
					{
						alert('Erro ao banner principal.');
						redirect("gerencia_banner_principal.php");	
					}
				}
			}
		
		}
		else
		{				
			$query	="update banner_principal set nome = '$nome', inicio = '$data_entrada', fim = '$data_saida', ordem = '$ordem' $desativa_status where id = $id_banner ";
			$result = mysql_query($query);
			
			if($result)
			{
				alert('Banner principal alterado com sucesso.');
				redirect("gerencia_banner_principal.php");	
		 	}
		 	else
		 	{
		 		alert('Erro ao banner principal.');
				redirect("gerencia_banner_principal.php");	
		 	}
		}
		
	}
	else ////////////////// abre a pagina \\\\\\\\\\\\\\\
	{
		$conteudo_tpl 	= AbrePag(DIR_TEMPLATES.'editar_banner_principal.html');
		$conteudo_tpl 	= str_replace ('##ACTION##', 'editar_banner_principal.php', $conteudo_tpl);
		
		$id_banner = $_GET['id_banner'];
		
		$query	= "select * from banner_principal where id = $id_banner";
		$result	= mysql_query($query);
		$row = mysql_fetch_array($result);
		
		$nome 			= $row['nome'];
		$dataEntrada 	= saidaData($row['inicio']);
		$dataSaida 		= saidaData($row['fim']);
		$flash			= $row['flash'];
		$ordem			= $row['ordem'];
		
		if($nome == 'padrao')
			$disabled = 'disabled';
		else
			$disabled = ' ';
		
		//selecionando a ordem dispon�vel para exibi��o
		$query 	= "select ordem from banner_principal where id = $id_banner";
		$result	= mysql_query($query);
		$ordem	= mysql_result($result,0,'ordem');

		$query_count 	= "select count(id) as totalOpcoes from banner_principal order by ordem";
		$result_count 	= mysql_query($query_count);
		$totalOpcoes 	= mysql_result($result_count,0,'totalOpcoes');

		$limite = $totalOpcoes + 5;
		$i=1;
		$montaOrdem = array();
		$novoArray = array();
				
		for($j=1;$j<=$limite;$j++)
			$montaOrdem[] = $j;	
			
		$query_ordem 	= "select * from banner_principal where ordem != '".$ordem."' order by ordem";
		$result_ordem	= mysql_query($query_ordem);

								
		while($row_ordem=mysql_fetch_array($result_ordem)){
			$key = array_search($row_ordem['ordem'], $montaOrdem); 
			if(isset($key))
				unset($montaOrdem[$key]);
			
			$i++;
		}

		foreach($montaOrdem as $key => $value)
			$novoArray[] = $value;
						
		$ordem_disponivel = "<select name='ordem'>";
		
		for($i=0;$i<count($novoArray);$i++)
		{	
			if($ordem == $novoArray[$i])
				$ordem_disponivel .= "<option value='$ordem' selected>$ordem</option>";
			else
				$ordem_disponivel .= "<option value='$novoArray[$i]'>$novoArray[$i]</option>";	
		}
		$ordem_disponivel .= "</select>";
		
		$conteudo_tpl 	= str_replace ('##ORDENACAO##', $ordem_disponivel, $conteudo_tpl);
		
		$conteudo_tpl		 = str_replace ("##DISABLED##" , $disabled , $conteudo_tpl);
		
		$submit_id 			 = "<input name='id_banner' type='hidden' value='".$id_banner."' />";
		$conteudo_tpl		 = str_replace ("##HIDDEN##" , $submit_id , $conteudo_tpl);
		$conteudo_tpl		 = str_replace ("##NOME##" , $nome , $conteudo_tpl);
		$conteudo_tpl		 = str_replace ("##DATAENTRADA##" , $dataEntrada , $conteudo_tpl);
		$conteudo_tpl		 = str_replace ("##DATASAIDA##" , $dataSaida , $conteudo_tpl);
		
		////////////////////////////////////banner para exibição na página/////////////////////////////
		
		$banner = "<script src='../../Scripts/swfobject_modified.js' type='text/javascript'></script>
<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' width='215' height='206' id='FlashID' title='bannerPrincipal'>
          <param name='movie' value='../../swf/".$flash."?".rand()."'>
          <param name='quality' value='high'>
          <param name='wmode' value='opaque'>
          <param name='swfversion' value='8.0.35.0'>
          <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
          <param name='expressinstall' value='../../Scripts/expressInstall.swf'>
          <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
          <!--[if !IE]>-->
          <object type='application/x-shockwave-flash' data='../../images/banner/principal/".$flash."?".rand()."' width='500' height='206'>
            <!--<![endif]-->
            <param name='quality' value='high'>
            <param name='wmode' value='opaque'>
            <param name='swfversion' value='8.0.35.0'>
            <param name='expressinstall' value='../../Scripts/expressInstall.swf'>
            <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
            <div>
              <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
              <p><a href='http://www.adobe.com/go/getflashplayer'><img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' width='112' height='33' /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>";
		
		$conteudo_tpl		 = str_replace ("##BANNER##" , $banner , $conteudo_tpl);
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		
		include("../navegacao.php");
		?>
        
        
		<div class="conteudo">
		<?
			echo $conteudo_tpl;
		?>
		</div> 
		<?
	}////////////////////////////////////////////////////////////////
}

else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	require_once("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
        </script>
