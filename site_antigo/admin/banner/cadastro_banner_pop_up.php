<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();

if(verificaPermissao("permissao_criar_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	$submit = $_POST['submit'];
	
	if(isset($submit))
	{
		/*$data 	= $_POST['data'];*/
		$nome 			= $_POST['nome'];
		$data_entrada 	= entradaData($_POST['dataEntrada']);
		$data_saida		= entradaData($_POST['dataSaida']);
		$alturaBanner	= $_POST['alturaBanner'];
		$latguraBanner	= $_POST['latguraBanner'];
		$data = dataAtual();
		
		$ativa = $_POST['ativa'];
		
		$data_atual = date('Ymd');
		///retirando os "-" da data para depois ser feita a compara��o////////////////////////////////////////////////////////////
		$data_fim_numeral = str_replace("-", "",  $data_saida);
		
		if(($ativa == "on") and ($data_fim_numeral <= $data_atual))
		{ 
			alert("O banner n�o foi ativado pois sua data de sa�da j� foi expirada!");
			$ativa = 0;
		}
		elseif($ativa == "on")
		{
			$ativa = 1;
		}
		else
			$ativa = 0;

		//verificando se o arquivo importado � um swf, se n�o o usu�rio � redirecionado para p�gina de listagem sem cadastrar o arquivo
		if($_FILES['arquivo']['type'] != "application/x-shockwave-flash")
		{
			alert("Apenas arquivos com extens�o swf s�o aceitas");
			redirect("gerencia_banner_pop_up.php");
		}
		else
		{
			$query	= "insert into banner_pop_up(nome,inicio,fim,flash,altura,largura,status) values('$nome','$data_entrada','$data_saida','','$alturaBanner','$latguraBanner','$ativa')";
			
			$result = mysql_query($query);
			
			if($result)
			{
				$id_banner = mysql_insert_id();				
				
				if($_FILES['arquivo']['name'] != "")
				{
					$nome_banner 	= basename($_FILES['arquivo']['name']);
					$ext 			= explode('.', $nome_banner);
					$extensao		= strtolower($ext[1]);
					
					$path 		= "../../swf/pop_up/".$id_banner."_".$ext[0].".".$ext[1];
					
					$arquivo	= $id_banner."_".$ext[0].".".$ext[1];
					
					if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $path )) 
					{
						$query2 ="update banner_pop_up set flash='$arquivo' where id = '$id_banner' ";
						$result2 = mysql_query($query2);
					}
					
				}
				alert('Banner principal adicionado com sucesso.');
				redirect("gerencia_banner_pop_up.php");	
			 }//fim do result
			 else
			 {
				alert('Erro ao adicionar banner principal.');
				redirect("gerencia_banner_pop_up.php");	
			 }
		}
	
	}
	else ////////////////// abre a pagina \\\\\\\\\\\\\\\
	{
		$conteudo_tpl 	= AbrePag(DIR_TEMPLATES.'cadastro_banner_pop_up.html');
		$conteudo_tpl 	= str_replace ('##ACTION##', 'cadastro_banner_pop_up.php', $conteudo_tpl);
	
		$ativarHome = "<tr>
        	<td>Ativar banner na index do site:</td>
            <td><input type='checkbox' name='ativa' id='ativa' /></td>
        </tr>";
		
		$query_select	= "select * from banner_pop_up where status = 1";
		$result_select	= mysql_query($query_select);
		$num_row		= mysql_num_rows($result_select);
		
		if($num_row > 0)
			$conteudo_tpl 	= str_replace ('##ATIVA_HOME##', "", $conteudo_tpl);
		else
			$conteudo_tpl 	= str_replace ('##ATIVA_HOME##', $ativarHome, $conteudo_tpl);
		////////////////////////////////////////////////////////////////
		
		include("../navegacao.php");
		?>
		<div class="conteudo">
		<?
			echo $conteudo_tpl;
		?>
		</div> 
		<?
		
		////////////////////////////////////////////////////////////////
	}
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	require_once("../rodape.php");
	////////////////////////////////////////////////////////////////
?>