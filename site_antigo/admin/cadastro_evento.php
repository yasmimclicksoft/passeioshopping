<? include("topo.php");
////////////////////////////////////////
include("../includes/functions.php");
include("../fckeditor/fckeditor.php");

conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$submit = $_POST['submit'];

	if (isset($submit))
	{
		// pega variaveis
		$nome 			= $_POST['nome'];
		$descricao 		= htmlentities($_POST['descricao']);
		$localizacao 	= $_POST['localizacao'];
		$data_inicio	= $_POST['data_inicio'];
		$data_fim		= $_POST['data_fim'];
		
		if( empty($nome) and empty($localizacao) )
		{
			alert('Preencha o nome e localiza��o do Evento.');
			die(redirect('cadastro_evento.php'));
		}
		/*if($data_fim < $data_inicio)
		{
			alert('A data final tem que ser maior que a data de inicio.');
			die(redirect('cadastro_evento.php'));
		}*/
		$pega_data_in = explode(" ",$data_inicio);
		$pega_data_in = explode("/",$pega_data_in[0]);
	
		$nova_data_in = $pega_data_in[2].$pega_data_in[1].$pega_data_in[0];
		
		$pega_data_f = explode(" ",$data_fim);
		$pega_data_f = explode("/",$pega_data_f[0]);
	
		$nova_data_f = $pega_data_f[2].$pega_data_f[1].$pega_data_f[0];
		
		////////////////////////////////////////////////////////////////////////////////
		
		//// separando a data da hora
		$data_inicio = explode(" ",$data_inicio);
		$data_ini 	 = $data_inicio[0];
		$hora_ini 	 = $data_inicio[1];
		
		$data_fim 	 = explode(" ",$data_fim);
		$data_f 	 = $data_fim[0];
		$hora_f 	 = $data_fim[1];
			
		///// formatando a data para gravar no banco
		$data_inicio = entradaData($data_ini);
		$data_inicio .= " ".$hora_ini.":00";
		
		$data_fim = entradaData($data_f);
		$data_fim .= " ".$hora_f.":00";
		
		$dti_ev_novo = $data_inicio;
		$dtf_ev_novo = $data_fim;
		
		////// verificando se j� existe evento no intervalo (suspenso por enquanto)\\\\\\\\\
			
		/*$query_data = "select * from eventos";
		$result_data = mysql_query($query_data);*/
		$cont = 1;//$cont = 0;
		/*while($row_data = mysql_fetch_array($result_data))
		{
			if($dti_ev_novo>$row_data['data_inicio'] and $dti_ev_novo>$row_data['data_fim'])
			{
				if($dtf_ev_novo > $row_data['data_inicio'] and $dtf_ev_novo > $row_data['data_fim'])
				{
					$cont++;
				}
				else
				{
					alert('J� existe evento neste hor�rio.');
					die(redirect('cadastro_evento.php'));
				}
			}
			else
			{
				alert('J� existe evento neste hor�rio.');
				die(redirect('cadastro_evento.php'));
			}
		}*/
		
		if($cont > 0)
		{				
			$query = "INSERT INTO eventos(nome,descricao,localizacao,data_inicio,data_fim,status) VALUES ('".$nome."', '".$descricao."', '".$localizacao."', '".$data_inicio."', '".$data_fim."', 0 )";
			//echo $query;
			$result = mysql_query ($query);
		
			if($result)
			{
				$id_evento = mysql_insert_id();
				
				if($_FILES['foto']['name'] != "")
				{
					$nome_imagem 	= basename($_FILES['foto']['name']);
					$ext 			= explode('.', $nome_imagem);
					$extensao		= strtolower($ext[1]);
					
					if($extensao != "jpg")
					{
						alert('Apenas imagens de extens�o jpg s�o aceitas. A imagem n�o p�de ser cadastrada.');
						redirect("gerencia_eventos.php");	
					}
					else
					{
						$query_imagem = "insert into imagens_eventos(nome,banner,evento_id) values('','','$id_evento')";
						$result_imagem = mysql_query($query_imagem);
						//$id_imagem = mysql_insert_id();
						
						$id_banner = mysql_insert_id();
						
						//$path = "../images/eventos/".$id_evento."_".$id_imagem.".".$extensao;
						$path_banner = "../images/eventos/banners/".$id_evento."_".$id_banner.".".$extensao;
						//$foto = $id_evento."_".$id_imagem.".".$extensao;
						$banner = $id_evento."_".$id_banner.".".$extensao;
						
						//if (move_uploaded_file($_FILES['foto']['tmp_name'], $path )) 
						if (move_uploaded_file($_FILES['foto']['tmp_name'], $path_banner )) 
						{
						  ###Estava OFF antes
							//thumbit ($path,100,100,S);
							//thumbit ($path,320,240,N);// para fotos
							###Estava on antes
							//thumbit ($path_banner,100,100,S);
							//thumbit ($path_banner,410,160,N);//para banner
							
							//$query2 = "update imagens_eventos set nome = '$foto' where id = '$id_imagem' ";
							$query2 = "update imagens_eventos set banner = '$banner' where id = '$id_banner' ";
							$result2 = mysql_query($query2);
							//echo $query2;
						}
					}
				}
				
				alert('Cadastro realizado com sucesso');
				redirect("gerencia_eventos.php");		
			}
			else
			{
				alert('Erro no cadastro');
				redirect('gerencia_eventos.php');
			}
		}	
		
	}
	else/////////////////////////////////////////////////////////////////////////////////
	{
		$conteudo_tpl 		= AbrePag(DIR_TEMPLATES.'cadastro_evento.html');
		$conteudo_tpl 		= str_replace ('##ACTION##', 'cadastro_evento.php', $conteudo_tpl);
		
		// inserindo o editor de html
		$sBasePath = $_SERVER['PHP_SELF'];
		$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, '_samples' ) );
		$oFCKeditor = new FCKeditor('descricao'); 
		$oFCKeditor->BasePath = '../fckeditor/';
		$oFCKeditor->Value = $descricao; 
		$oFCKeditor->width = '100%';
		$oFCKeditor->Height = '450'; 
		$descricao = $oFCKeditor->Create();
		//fui em fckeditor e alterei a funcao Create() de echo para return
		$conteudo_tpl 		= str_replace ('##DESCRICAO##', $descricao , $conteudo_tpl);
		
		$upload_html_form 	= "<input class='label' name = 'foto' type='file' />";
		$conteudo_tpl 		= str_replace ('##UPLOAD_FOTO##', $upload_html_form, $conteudo_tpl);
		
			
	////////////////////////////////////////////////////////////////
	include("navegacao.php");
	?>
	<div class="conteudo">
	<?
	echo $conteudo_tpl;
	?>
	</div> 
	<?
	
	include("rodape.php");
	////////////////////////////////////////////////////////////////
	}

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
?>