<? include("topo.php");
////////////////////////////////////////
include("../includes/functions.php");
include("../fckeditor/fckeditor.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$submit = $_POST['submit'];

if (isset($submit))
{
	$id_novidade	= $_POST['id_novidade'];

	$nome 			= $_POST['nome'];
	$titulo			= $_POST['titulo'];
	$descricao 		= $_POST['descricao'];
	$data_inicio	= entradaDataHora($_POST['data_inicio']);
 	$data_fim		= entradaDataHora($_POST['data_fim']);
	$conteudo 		= htmlentities($_POST['conteudo']);
	$homeDestaque	= $_POST['homeDestaque'];
	$homeNovidade	= $_POST['homeNovidade'];
		
	if(!isset($homeDestaque)) $homeDestaque = 0;
	if(!isset($homeNovidade)) $homeNovidade = 0;
		
	if(!isset($home)) $home = 0;
				
	if( empty($nome))
	{
		alert('Preencha um nome para novidade que deseja editar.');
		die(redirect("edita_novidade.php?id_novidade=".$id_novidade));
	}
	

	if($_FILES['imagem']['name'] != "")
	{
		$nome_imagem 	= basename($_FILES['imagem']['name']);
		$ext 			= explode('.', $nome_imagem);
		$extensao		= strtolower($ext[1]);
		
		//echo $nome_imagem;
		//exit;

		if(($extensao != "jpg") && ($extensao != "png"))
		{
			alert('Apenas imagens de extens�o jpg e png s�o aceitas. A imagem n�o p�de ser cadastrada.');
			redirect("edita_novidade.php?id_novidade=".$id_novidade);	
		}
		else
		{
			$path = "../images/novidades/".$id_novidade.".".$extensao;
			$imagem = $id_novidade.".".$extensao;

						
			if (move_uploaded_file($_FILES['imagem']['tmp_name'], $path )) 
			{

				thumbit ($path,151,71,S);// imagem do box da home
				//thumbit ($path,320,240,N);
				thumbit($path,303,142,N);
				
				$query2= "update novidades set imagem= '$imagem' where id= '$id_novidade' ";
				$result2= mysql_query($query2);
				//echo $query2;
			}
		}
	}
	
	$query_update = "update novidades set nome= '$nome', titulo = '$titulo', descricao = '$descricao', inicio= '$data_inicio', fim= '$data_fim', conteudo= '$conteudo', homeDestaque = '$homeDestaque', homeNovidade = '$homeNovidade'  where id = '$id_novidade' ";
	$result_update = mysql_query($query_update)	;
	//echo $query_update;
	if($result_update)
	{
		alert('Altera��o realizada com sucesso');
		redirect("gerencia_novidades.php");	
	}
	else
	{
		alert('Erro ao alterar.');
		redirect("edita_novidade.php?id_novidade=".$id_novidade);
	}
}
else///////////////////////////////////////////////////////////////////////////////////
{
	$id_novidade  = $_GET['id_novidade'];
	
	$conteudo_tpl = AbrePag(DIR_TEMPLATES.'edita_novidade.html');
	$conteudo_tpl = str_replace ('##ACTION##' , 'edita_novidade.php' , $conteudo_tpl);
	
	$query 		  = "select * from novidades where id = $id_novidade";
	$result 	  = mysql_query($query);
	$row 		  = mysql_fetch_array($result);
					//funcao que recupera a string gravada com tags no banco
	//$nome			= stripcslashes($row['nome']);
	$nome		  = $row['nome'];
	$titulo		  = $row['titulo'];
	$descricao	  = $row['descricao'];
	
	$submit_id 	  = "<input name = 'id_novidade' type = 'hidden' value = '".$id_novidade."'/>";
	$conteudo_tpl = str_replace ("##HIDDEN##" , $submit_id , $conteudo_tpl);
	$conteudo_tpl = str_replace ("##NOME##" , $nome , $conteudo_tpl);
	$conteudo_tpl = str_replace ("##TITULO##" , $titulo , $conteudo_tpl);
	$conteudo_tpl = str_replace ('##DESCRICAO##' , $descricao , $conteudo_tpl);
	$conteudo_tpl = str_replace ('##DATA_INICIO##' , saidaDataHoraCal($row['inicio']) , $conteudo_tpl);
	$conteudo_tpl = str_replace ('##DATA_FIM##' , saidaDataHoraCal($row['fim']) , $conteudo_tpl);
	
	if($row['homeDestaque'] == 1){
		$conteudo_tpl	 = str_replace ("##CHECK_DESTAQUE##" , "checked='checked'"  , $conteudo_tpl);
	}else{
		$conteudo_tpl	 = str_replace ("##CHECK_DESTAQUE##" , ""  , $conteudo_tpl);
	}
	if($row['homeNovidade'] == 1){
		$conteudo_tpl	 = str_replace ("##CHECK_NOVIDADE##" , "checked='checked'"  , $conteudo_tpl);
	}else{
		$conteudo_tpl	 = str_replace ("##CHECK_NOVIDADE##" , ""  , $conteudo_tpl);
	}
	
	// inserindo o editor de html
	$sBasePath = $_SERVER['PHP_SELF'];
	$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, '_samples' ) );
	$oFCKeditor = new FCKeditor('conteudo'); 
	$oFCKeditor->BasePath = '../fckeditor/';
	$oFCKeditor->Value = html_entity_decode($row['conteudo']); 
	$oFCKeditor->width = '100%';
	$oFCKeditor->Height = '450'; 
	$conteudo = $oFCKeditor->Create();
	
	$conteudo_tpl	= str_replace ('##CONTEUDO##' , $conteudo , $conteudo_tpl);
	
	$upload_imagem	 	= "<input class='label' name = 'imagem' type='file' />";
	$conteudo_tpl 		= str_replace ('##UPLOAD_IMAGEM##', $upload_imagem , $conteudo_tpl);
	
		
	if($row['imagem'] > 0)
	{
		$imagem = $row['imagem'];
		$imagem_mini = explode(".", $imagem);
		$imagem_mini = $imagem_mini[0]."_mini.".$imagem_mini[1];
		
		$imagem = "<img src='../images/novidades/$imagem_mini '/>";
		$conteudo_tpl 	 = str_replace("##IMAGEM##", $imagem , $conteudo_tpl);
		$acao	="<a class=txt_pag href='exclui_novidade.php?id_novidade=$id_novidade&acao=excluir_imagem'>excluir imagem</a>";
		$conteudo_tpl 	 = str_replace("##EXCLUIR_IMAGEM##", $acao , $conteudo_tpl);
	}
	else
	{
		$conteudo_tpl 	 = str_replace("##IMAGEM##", '' , $conteudo_tpl);
		$conteudo_tpl 	 = str_replace("##EXCLUIR_IMAGEM##", '' , $conteudo_tpl);
	}
	////////////////////////////////////////////////////////////////
	include("navegacao.php");
?>
	<div class="conteudo">
<?
	echo $conteudo_tpl;
?>
	</div> 
<?
	include("rodape.php");
	////////////////////////////////////////////////////////////////
}

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
?>