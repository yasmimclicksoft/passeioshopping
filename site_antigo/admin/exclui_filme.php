<? include("topo.php");
////////////////////////////////////////
include("../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$id_filme 	= $_GET['id_filme'];
	$acao		= $_GET['acao'];

	$query 		= "SELECT * FROM filmes WHERE id = ".$id_filme;
	$result 	= mysql_query($query);
	$row 		= mysql_fetch_array($result);
	
	$nome 		= $row['nome'];
	$imagem 	= $row['imagem'];
	$trailler 	= $row['trailler'];
	
	if($acao == 'excluir_imagem')
	{
		$mini = explode(".", $imagem);
		$imagem_mini = $mini[0]."_mini".".".$mini[1];
		$imagem_flash = $mini[0]."_flash".".".$mini[1];
		@unlink("../filmes/imagens/".$imagem);
		@unlink("../filmes/imagens/".$imagem_mini);
		@unlink("../filmes/imagens/".$imagem_flash);
		
		$query_imagem = "update filmes set imagem = '' where id = $id_filme";
		$result_imagem= mysql_query($query_imagem);
		if($result_imagem)
		{
			alert('Imagem excluida com sucesso.');
			die(redirect("edita_filme.php?id_filme=$id_filme"));
		}
	}
	if($acao == 'excluir_trailler')
	{
		@unlink("../filmes/traillers/".$trailler);
		
		$query_trailler = "update filmes set trailler = '' where id = $id_filme";
		$result_trailler= mysql_query($query_trailler);
		if($result_trailler)
		{
			alert('Trailler excluido com sucesso.');
			die(redirect("edita_filme.php?id_filme=$id_filme"));
		}
	}
	
	
	$query_del	= "DELETE FROM filmes WHERE id = '".$row['id']."' ";
	$result_del = mysql_query($query_del);
	
	if($result_del)
	{
		//apaga imagem se houver
		if( $imagem != "" )
		{
			$mini = explode(".", $imagem);
			$imagem_mini = $mini[0]."_mini".".".$mini[1];
			@unlink("../filmes/imagens/".$imagem);
			@unlink("../filmes/imagens/".$imagem_mini);
		}
		//apaga trailler se houver
		if( $trailler != "" )
		{
			@unlink("../filmes/traillers/".$trailler);
		}
		//apaga sessoes se houver
		$querySessoes = "select * from sessoes where filme_id = ".$id_filme." ";
		$resultSessoes= mysql_query($querySessoes);
		if($resultSessoes)
		{
			while($rowSessoes = mysql_fetch_array($resultSessoes))
			{
				$query_del_horarios = "delete from sessoes_horarios where sessao_id = ".$rowSessoes['id']." ";
				@$result_del_horarios = mysql_query($query_del_horarios);
			}
			$query_del_sessoao = "delete from sessoes where filme_id = ".$id_filme." ";
			@$result_del_sessoao= mysql_query($query_del_sessoao);
		}
		if(result_del_sessoao)
		{
			alert('Filme excluido com sucesso.');
			redirect("gerencia_filmes.php");
		}
	}
	else
	{
		alert('Erro ao excluir filme.');
		redirect("gerencia_filmes.php");
	}
	
	////////////////////////////////////////////////////////////////
		include("navegacao.php");
	?>
		<div class="conteudo">
	<?
		echo $conteudo_tpl;
	?>
		</div> 
	<?
		include("rodape.php");
		////////////////////////////////////////////////////////////////

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
?>
