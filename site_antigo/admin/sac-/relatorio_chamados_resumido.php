<? include("../topo.php"); 
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
//$id_user = $_GET['id_user'];
//echo $_SESSION['id_usuario'];
if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	$submit = $_POST['submit'];
	$idsac	= $_POST['idsac'];
	$idsac2 = $_POST['idsac2'];
	$lojas  = $_POST['lojas'];
	$areas  = $_POST['areas'];
	
	
	//colocando em sessão para ser mantido no input apos a busca//3
	if($_POST['data_inicio'] != "")
		$_SESSION['dataInicioSession'] = $_POST['data_inicio'];
		
	if($_POST['data_fim'] != "")
		$_SESSION['dataFimSession'] = $_POST['data_fim'];
		
	$data_inicio = entradaDataHoraInicio($_SESSION['dataInicioSession']);
	$data_fim = entradaDataHoraFinal($_SESSION['dataFimSession']);
	
	if($submit or isset($_GET['order']))
	{
		/////////////////////////////condição para seleção do assunto////////////////////////////////////////////			
			if($idsac2 == idsac2_elogio){
				$condicao2 = " and (ch.elogio_id > 0 or ch.assunto = 'elogio') "; 	
			}
			elseif($idsac2 == idsac2_reclamacao){
				$condicao2 = " and ch.assunto = 'reclama��o' ";
			}
			elseif($idsac2 == idsac2_sugestao){
				$condicao2 = " and ch.sugestao = 'sugestao' ";
			}
			else{
				$condicao2 = "";
			}
///////////////////////////////////////////////////////////////////////////////////////////////////////////		
///////////////////////////////////////////////quando selecionado a �rea/////////////////////////////////////////////////
		if($idsac == idsac_areas){
			
			
			if($areas != 0 ){
				$condicao = " and ch.area_id = ".$areas;
			}
			
			else{
				$condicao = "";
			}	

////////////////////////////////////////////soma dos chamados por �rea//////////////////////////////
 		 $query = "select 
					  ch.area_id,
					  a.nome as nomeArea,
					  count(ch.area_id) as quantidadeChamados, 
					  ch.data_criacao, 
					  ch.data_fechamento
				  from 
					  chamados ch, 
					  areas a
				  where 
					  ch.area_id = a.id 
					  ".$condicao."
					  and ch.data_criacao >= '$data_inicio'
					  and ch.data_criacao <= '$data_fim'
					  and ch.data_fechamento <= '$data_fim' 
					  ".$condicao2."
				  group by 
					  ch.area_id
				  order by quantidadeChamados desc, nomeArea asc";
				 
		$result = mysql_query($query);
////////////////////////////////////////////////////////////////soma dos chamados abertos////////////////////////
	$query2 ="
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeAbertos, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAbertos,
					timediff(now(),ch.data_criacao) as diferencaHotaAbertos,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAbertos,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAbertos,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAbertos
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio'
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'aberto'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeAbertos desc, nomeArea asc";
					
		 $result2 = mysql_query($query2);
////////////////////////////////////////////////////////////////soma dos chamados em andamento////////////////////////
		$query3 ="
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeAndamento, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAndamento,
					timediff(now(),ch.data_criacao) as diferencaHotaAndamento,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAndamento,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAndamento,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAndamento
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'em andamento'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeAndamento desc, nomeArea asc";
		
		 $result3 = mysql_query($query3);
/////////////////////////////////////////////////////////////soma dos chamados concluídos//////////////////////////////
$query4 ="
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeConcluido, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(ch.data_fechamento,ch.data_criacao) as diferencaDataConcluido,
					timediff(ch.data_fechamento,ch.data_criacao) as diferencaHotaConcluido,
					TIME_FORMAT(timediff(ch.data_fechamento,ch.data_criacao),'%k') DIV 24 as diasconcluido,
					MOD(TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%k'),24) as horasconcluido,
					TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%i') as minutosconcluido
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'concluido'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeConcluido desc, nomeArea asc";
					
		 $result4 = mysql_query($query4);
		 
		 
		$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'relatorio_chamados_resumido.html');	
	
		$mensagem = 'Este per&iacute;odo n&atilde;o consta com nenhum chamado nesta classifica&ccedil;&atilde;o';
		$numrows	= mysql_num_rows($result);
		
		if($numrows > 0)
		{
			$totalChamados = 0;
			$totalabertos = 0;
			$totalAndamento = 0;
			$totalConcluido = 0;
			$totalDataAbertos = 0;
			$totalDataAndamento = 0;
			$totalDataConcluido = 0;
			
			$campos= "";
			while ($row = mysql_fetch_array($result)) 
			{		
				$totalConcluido = 0;
				$totalDataAbertos = 0;
				$totalDataAndamento = 0;
				$totalDataConcluido = 0;
				
				$tempoAbertoDias = 0;
				$tempoAbertoHoras = 0;
				$tempoAbertoMinutos = 0;
				
				$tempoAndamentoDias = 0;
				$tempoAndamentoHoras = 0;
				$tempoAndamentoMinutos = 0;
				
				$tempoConcluidoDias = 0;
				$tempoConcluidoHoras = 0;
				$tempoConcluidoMinutos = 0;
			
				$nomeArea				= $row['nomeArea'];
				$quantidadeChamados		= $row['quantidadeChamados'];
				$data_criacao			= $row['data_criacao'];
				$data_fechamento		= $row['data_fechamento'];
				
				$totalChamados += $quantidadeChamados;
				$campos	.= "<tr class='tr_txt txt_pag'>";
				$campos .= "<td class='td_txt'>".$nomeArea."</td>";
				$campos .= "<td class='td_txt'>".$quantidadeChamados."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAbertos=0;
				///////////////////////////////////////////////////////////////
				//TIVE QUE IMPLEMENTAR DESTA FORMA POIS A BASE ESTÁ TODA ZONEADA --> BY JONATHAS E DANIEL COM AUTORIZAÇÃO DO CARLOS
				while ($row2 = mysql_fetch_array($result2)){
					if ($row['area_id']==$row2['area_id']){
						$quantidadeAbertos 		= $row2['quantidadeAbertos'];
						$diferencaDataAbertos	= $row2['diferencaDataAbertos'];
						$diferencaHotaAbertos	= $row2['diferencaHotaAbertos'];
						$diasAbertos			= $row2['diasAbertos'];
						$horasAbertos			= $row2['horasAbertos'];
						$minutosAbertos			= $row2['minutosAbertos'];
						$tempoAbertoDias += $diasAbertos;
						$tempoAbertoHoras += $horasAbertos;
						$tempoAbertoMinutos += $minutosAbertos;
						$totalDataAbertos += $diferencaDataAbertos;
						$totalabertos += $quantidadeAbertos;
						break;
					}
				}
				@mysql_data_seek($result2,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAbertos."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAndamento=0;
				///////////////////////////////////////////////////////////////
				while ($row3 = mysql_fetch_array($result3)){
					if ($row['area_id']==$row3['area_id']){
						$quantidadeAndamento 	= $row3['quantidadeAndamento'];
						$diferencaDataAndamento	= $row3['diferencaDataAndamento'];
						$diferencaHotaAndamento	= $row3['diferencaHotaAndamento'];
						$diasAndamento			= $row3['diasAndamento'];
						$horasAndamento			= $row3['horasAndamento'];
						$minutosAndamento			= $row3['minutosAndamento'];
						$tempoAndamentoDias += $diasAndamento;
						$tempoAndamentoHoras += $horasAndamento;
						$tempoAndamentoMinutos += $minutosAndamento;
						$totalDataAndamento += $diferencaDataAndamento;
						$totalAndamento += $quantidadeAndamento;
						break;
					}
				}
				@mysql_data_seek($result3,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAndamento."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeConcluido=0;
				///////////////////////////////////////////////////////////////
				while ($row4 = mysql_fetch_array($result4)){
					if ($row['area_id']==$row4['area_id']){
						$quantidadeConcluido 	= $row4['quantidadeConcluido'];
						$diferencaDataConcluido	= $row4['diferencaDataConcluido'];
						$diferencaHotaConcluido	= $row4['diferencaHotaConcluido'];
						$diasConcluido			= $row4['diasConcluido'];
						$horasConcluido			= $row4['horasConcluido'];
						$minutosConcluido		= $row4['minutosConcluido'];
						$tempoConcluidoDias += $diasConcluido;
						$tempoConcluidoHoras += $horasConcluido;
						$tempoConcluidoMinutos += $minutosConcluido;
						$totalDataConcluido += $diferencaDataConcluido;
						$totalConcluido += $quantidadeConcluido;
						break;
					}
				}
				@mysql_data_seek($result4,0);	
				$totalTempo = 0;
				$totalTempo = $totalDataAbertos + $totalDataAndamento + $totalDataConcluido;
				$totalTempoGeral += $totalTempo;
				
				$totalTempoDia = 0;
				$totalTempoDia = $tempoAbertoDias + $tempoAndamentoDias + $tempoConcluidoDias;
				$totalTempoDiaGeral += $totalTempoDia;
				
				$totaltempoHora = 0;
				$totaltempoHora = $tempoAbertoHoras + $tempoAndamentoHoras + $tempoConcluidoHoras;
				$totalTempoHoraGeral += $totaltempoHora;
				
				$totaltempoMinuto = 0;
				$totaltempoMinuto = $tempoAbertoMinutos + $tempoAndamentoMinutos + $tempoConcluidoMinutos;
				$totalTempoMinutoGeral += $totaltempoMinuto;
				
				$campos .= "<td class='td_txt'>".$quantidadeConcluido."</td>";
				//$campos .= "<td class='td_txt'>".$totalTempo." dia(s)</td>";
				$campos .= "<td class='td_txt'>".$totalTempoDia." dia(s) ".$totaltempoHora." hora(s) ".$totaltempoMinuto." minuto(s)</td>";
				$campos .= "</tr>";
				
			}
			$campoMedia .= "<tr class='tr_tit txt_tit'><td class='td_txt' align='left' colspan='6'>Total Geral</td></tr>";
			$campoMedia .= "<tr class='tr_txt txt_pag'>		
	   								<td class='td_txt'><b>Total:</b></td>
									<td class='td_txt'>".$totalChamados."</td>
									<td class='td_txt'>".$totalabertos."</td>
									<td class='td_txt'>".$totalAndamento."</td>
									<td class='td_txt'>".$totalConcluido."</td>
									<td class='td_txt'>".$totalTempoDiaGeral." dia(s) ".$totalTempoHoraGeral." hora(s) ".$totalTempoMinutoGeral." minuto(s)</td>
								</tr>";
			
		}
		else
		{
			$campos 		.= "<tr class='tr_txt txt_pag'>
									<td class='td_txt' colspan='6'>".$mensagem."</td>
									
									
								</tr> ";
		}
		
		
		
		$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
		$conteudo_tpl		= str_replace ('##CAMPOMEDIA##', $campoMedia ,$conteudo_tpl);
		}
		//////////////////////////////////////////////////selecionando a loja/////////////////////////////////////////
		
		if($idsac == idsac_lojas){
			
		if($lojas != 0 ){
				$condicao = " and ch.loja_id = ".$lojas; 
			}
			
			else{
				$condicao = "";
			}
 ////////////////////////////////////////////soma dos chamados por área//////////////////////////////
 		$query = "select 
					 ch.loja_id, 
					 l.nome as nomeLoja, 
					 ch.data_criacao, 
					 ch.data_fechamento,
					 ch.assunto as nomeAssunto,
					 count(ch.assunto) as quantidadeChamados
				  from 
					  chamados ch, 
					  lojas l
				  where 
					  ch.loja_id = l.id 
					  ".$condicao." 
					  and ch.data_criacao >= '$data_inicio'
					  and ch.data_criacao <= '$data_fim'
					  and ch.data_fechamento <= '$data_fim'
					  ".$condicao2."
				  group by 
					  	ch.assunto
				  order by quantidadeChamados desc, nomeLoja asc";		  
		$result = mysql_query($query);
////////////////////////////////////////////////////////////////soma dos chamados abertos////////////////////////
$query2 ="
				select 
					ch.loja_id, 
					 l.nome as nomeLoja, 
					 ch.data_criacao, 
					 ch.data_fechamento,
					 ch.assunto as nomeAssunto,
					 count(ch.assunto) as quantidadeAbertos,
					 datediff(now(),ch.data_criacao) as diferencaDataAbertos,
					 timediff(now(),ch.data_criacao) as diferencaHotaAbertos,
					 TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAbertos,
					 MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAbertos,
					 TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAbertos
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao."
					and ch.data_criacao >= '$data_inicio'
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'aberto'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeAbertos desc, nomeLoja asc";
		 $result2 = mysql_query($query2);
////////////////////////////////////////////////////////////////soma dos chamados em andamento////////////////////////
$query3 ="
				select 
					ch.loja_id,
					l.nome as nomeLoja,
					ch.assunto as nomeAssunto,
					count(ch.assunto) as quantidadeAndamento, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAndamento,
					timediff(now(),ch.data_criacao) as diferencaHotaAndamento,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAndamento,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAndamento,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAndamento
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'em andamento'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeAndamento desc, nomeLoja asc";
		 $result3 = mysql_query($query3);
/////////////////////////////////////////////////////////////soma dos chamados concluídos//////////////////////////////
$query4 ="
				select 
					ch.loja_id,
					l.nome as nomeLoja,
					ch.assunto as nomeAssunto,
					count(ch.assunto) as quantidadeConcluido, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(ch.data_fechamento,ch.data_criacao) as diferencaDataConcluido,
					timediff(ch.data_fechamento,ch.data_criacao) as diferencaHotaConcluido,
					TIME_FORMAT(timediff(ch.data_fechamento,ch.data_criacao),'%k') DIV 24 as diasconcluido,
					MOD(TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%k'),24) as horasconcluido,
					TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%i') as minutosconcluido
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'concluido'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeConcluido desc, nomeLoja asc";
		 $result4 = mysql_query($query4);
		 
		 
		$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'relatorio_chamados_resumido.html');	
	
		$mensagem = 'Este per&iacute;odo n&atilde;o consta com nenhum chamado nesta classifica&ccedil;&atilde;o';
		$numrows	= mysql_num_rows($result);
		
		if($numrows > 0)
		{
			$totalChamados = 0;
			$totalabertos = 0;
			$totalAndamento = 0;
			$totalConcluido = 0;
			$totalDataAbertos = 0;
			$totalDataAndamento = 0;
			$totalDataConcluido = 0;
			
			$tempoAbertoDias = 0;
			$tempoAbertoHoras = 0;
			$tempoAbertoMinutos = 0;
			
			$tempoAndamentoDias = 0;
			$tempoAndamentoHoras = 0;
			$tempoAndamentoMinutos = 0;
			
			$tempoConcluidoDias = 0;
			$tempoConcluidoHoras = 0;
			$tempoConcluidoMinutos = 0;
			
			$campos= "";
			while ($row = mysql_fetch_array($result)) 
			{		
				$totalConcluido = 0;
				$totalDataAbertos = 0;
				$totalDataAndamento = 0;
				$totalDataConcluido = 0;
			
				$nomeLoja				= $row['nomeAssunto'];
				$quantidadeChamados		= $row['quantidadeChamados'];
				$data_criacao			= $row['data_criacao'];
				$data_fechamento		= $row['data_fechamento'];
				
				$totalChamados += $quantidadeChamados;
				$campos	.= "<tr class='tr_txt txt_pag'>";
				$campos .= "<td class='td_txt'>".$nomeLoja."</td>";
				$campos .= "<td class='td_txt'>".$quantidadeChamados."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAbertos=0;
				///////////////////////////////////////////////////////////////
				//TIVE QUE IMPLEMENTAR DESTA FORMA POIS A BASE ESTÁ TODA ZONEADA --> BY JONATHAS E DANIEL COM AUTORIZAÇÃO DO CARLOS
				while ($row2 = mysql_fetch_array($result2)){

					if ($row['nomeAssunto']==$row2['nomeAssunto']){
						$quantidadeAbertos = $row2['quantidadeAbertos'];
						$diferencaDataAbertos	= $row2['diferencaDataAbertos'];
						$diferencaHotaAbertos	= $row2['diferencaHotaAbertos'];
						$diasAbertos			= $row2['diasAbertos'];
						$horasAbertos			= $row2['horasAbertos'];
						$minutosAbertos			= $row2['minutosAbertos'];
						$tempoAbertoDias += $diasAbertos;
						$tempoAbertoHoras += $horasAbertos;
						$tempoAbertoMinutos += $minutosAbertos;
						$totalDataAbertos += $diferencaDataAbertos;
						$totalabertos += $quantidadeAbertos;
						break;
					}
				}
				@mysql_data_seek($result2,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAbertos."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAndamento=0;
				///////////////////////////////////////////////////////////////
				while ($row3 = mysql_fetch_array($result3)){
					if ($row['nomeAssunto']==$row3['nomeAssunto']){
						$quantidadeAndamento = $row3['quantidadeAndamento'];
						$diferencaDataAndamento	= $row3['diferencaDataAndamento'];
						$diferencaHotaAndamento	= $row3['diferencaHotaAndamento'];
						$diasAndamento			= $row3['diasAndamento'];
						$horasAndamento			= $row3['horasAndamento'];
						$minutosAndamento			= $row3['minutosAndamento'];
						$tempoAndamentoDias += $diasAndamento;
						$tempoAndamentoHoras += $horasAndamento;
						$tempoAndamentoMinutos += $minutosAndamento;
						$totalDataAndamento += $diferencaDataAndamento;
						$totalAndamento += $quantidadeAndamento;
						break;
					}
				}
				@mysql_data_seek($result3,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAndamento."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeConcluido=0;
				///////////////////////////////////////////////////////////////
				while ($row4 = mysql_fetch_array($result4)){
					if ($row['nomeAssunto']==$row4['nomeAssunto']){
						$quantidadeConcluido = $row4['quantidadeConcluido'];
						$diferencaDataConcluido	= $row4['diferencaDataConcluido'];
						$diferencaHotaConcluido	= $row4['diferencaHotaConcluido'];
						$diasConcluido			= $row4['diasConcluido'];
						$horasConcluido			= $row4['horasConcluido'];
						$minutosConcluido		= $row4['minutosConcluido'];
						$tempoConcluidoDias += $diasConcluido;
						$tempoConcluidoHoras += $horasConcluido;
						$tempoConcluidoMinutos += $minutosConcluido;
						$totalDataConcluido += $diferencaDataConcluido;
						$totalConcluido += $quantidadeConcluido;
						break;
					}
				}
				@mysql_data_seek($result4,0);
				$totalTempo = 0;
				$totalTempo = $totalDataAbertos + $totalDataAndamento + $totalDataConcluido;
				$totalTempoGeral += $totalTempo;
				
				$totalTempoDia = 0;
				$totalTempoDia = $tempoAbertoDias + $tempoAndamentoDias + $tempoConcluidoDias;
				$totalTempoDiaGeral += $totalTempoDia;
				
				$totaltempoHora = 0;
				$totaltempoHora = $tempoAbertoHoras + $tempoAndamentoHoras + $tempoConcluidoHoras;
				$totalTempoHoraGeral += $totaltempoHora;
				
				$totaltempoMinuto = 0;
				$totaltempoMinuto = $tempoAbertoMinutos + $tempoAndamentoMinutos + $tempoConcluidoMinutos;
				$totalTempoMinutoGeral += $totaltempoMinuto;
				
				$campos .= "<td class='td_txt'>".$quantidadeConcluido."</td>";
				//$campos .= "<td class='td_txt'>".$totalTempo." dia(s)</td>";
				
				$campos .= "<td class='td_txt'>".$totalTempoDia." dia(s) ".$totaltempoHora." hora(s) ".$totaltempoMinuto." minuto(s)</td>";
				
				$campos .= "</tr>";
				
			}
			$campoTotal .= "<tr class='tr_tit txt_tit'><td class='td_txt' align='left' colspan='6'>Total Geral</td></tr>";
			$campoTotal .= "<tr class='tr_txt txt_pag'>		
	   								<td class='td_txt'><b>Total:</b></td>
									<td class='td_txt'>".$totalChamados."</td>
									<td class='td_txt'>".$totalabertos."</td>
									<td class='td_txt'>".$totalAndamento."</td>
									<td class='td_txt'>".$totalConcluido."</td>
									<td class='td_txt'>".$totalTempoDiaGeral." dia(s) ".$totalTempoHoraGeral." hora(s) ".$totalTempoMinutoGeral." minuto(s)</td>
								</tr>";
			//}
		}
		else
		{
			$campos 		.= "<tr class='tr_txt txt_pag'>
									<td class='td_txt' colspan='6'>".$mensagem."</td>
									
									
								</tr> ";
		}
		
		
		$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
		$conteudo_tpl		= str_replace ('##CAMPOMEDIA##', $campoTotal ,$conteudo_tpl);
		}
		
/////////////////////////////exibir tanto area quanto loja/////////////////////////////////////////////////////////////////////
		if($idsac == idsac_todos){
			
 ////////////////////////////////////////////soma dos chamados por �rea e Loja//////////////////////////////
 	
	$query = "select 
					 ch.loja_id, 
					 l.nome as nomeLoja, 
					 ch.data_criacao, 
					 ch.data_fechamento,
					 ch.assunto as nomeAssunto,
					 count(ch.assunto) as quantidadeChamados
				  from 
					  chamados ch, 
					  lojas l
				  where 
					  ch.loja_id = l.id 
					  ".$condicao." 
					  and ch.data_criacao >= '$data_inicio' 
					  and ch.data_criacao <= '$data_fim'
					  and ch.data_fechamento <= '$data_fim'
					  ".$condicao2."
				  group by 
					  	ch.assunto
				  order by quantidadeChamados desc, nomeLoja asc";	
					  
		$result = mysql_query($query);
	
	$query_area = "select 
					  ch.area_id,
					  a.nome as nomeArea,
					  count(ch.area_id) as quantidadeChamados, 
					  ch.data_criacao, 
					  ch.data_fechamento
				  from 
					  chamados ch, 
					  areas a
				  where 
					  ch.area_id = a.id 
					  ".$condicao."
					  and ch.data_criacao >= '$data_inicio'
					  and ch.data_criacao <= '$data_fim'
					  and ch.data_fechamento <= '$data_fim' 
					  ".$condicao2."
				  group by 
					  ch.area_id
				  order by quantidadeChamados desc, nomeArea asc";
					  
		$result_area = mysql_query($query_area);
		
////////////////////////////////////////////////////////////////soma dos chamados abertos////////////////////////
		$query2 ="
				select 
					ch.loja_id, 
					 l.nome as nomeLoja, 
					 ch.data_criacao, 
					 ch.data_fechamento,
					 ch.assunto as nomeAssunto,
					 count(ch.assunto) as quantidadeAbertos,
					 datediff(now(),ch.data_criacao) as diferencaDataAbertos,
					 timediff(now(),ch.data_criacao) as diferencaHotaAbertos,
					 TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAbertos,
					 MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAbertos,
					 TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAbertos
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao."
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'aberto'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeAbertos desc, nomeLoja asc";
	
		 $result2 = mysql_query($query2);
		 
		$query_area2 = "
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeAbertos, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAbertos,
					timediff(now(),ch.data_criacao) as diferencaHotaAbertos,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAbertos,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAbertos,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAbertos
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'aberto'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeAbertos desc, nomeArea asc";
					  
		$result_area2 = mysql_query($query_area2);
////////////////////////////////////////////////////////////////soma dos chamados em andamento////////////////////////
		 $query3 ="
				select 
					ch.loja_id,
					l.nome as nomeLoja,
					ch.assunto as nomeAssunto,
					count(ch.assunto) as quantidadeAndamento, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAndamento,
					timediff(now(),ch.data_criacao) as diferencaHotaAndamento,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAndamento,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAndamento,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAndamento
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'em andamento'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeAndamento desc, nomeLoja asc";
	
		 $result3 = mysql_query($query3);
		 
		$query_area3 = "
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeAndamento, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(now(),ch.data_criacao) as diferencaDataAndamento,
					timediff(now(),ch.data_criacao) as diferencaHotaAndamento,
					TIME_FORMAT(timediff(now(),ch.data_criacao),'%k') DIV 24 as diasAndamento,
					MOD(TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%k'),24) as horasAndamento,
					TIME_FORMAT(TIMEDIFF(now(),ch.data_criacao),'%i') as minutosAndamento
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'em andamento'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeAndamento desc, nomeArea asc";
					  
		$result_area3 = mysql_query($query_area3);
/////////////////////////////////////////////////////////////soma dos chamados concluídos//////////////////////////////
$query4 =
"
				select 
					ch.loja_id,
					l.nome as nomeLoja,
					ch.assunto as nomeAssunto,
					count(ch.assunto) as quantidadeConcluido, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(ch.data_fechamento,ch.data_criacao) as diferencaDataConcluido,
					timediff(ch.data_fechamento,ch.data_criacao) as diferencaHotaConcluido,
					TIME_FORMAT(timediff(ch.data_fechamento,ch.data_criacao),'%k') DIV 24 as diasconcluido,
					MOD(TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%k'),24) as horasconcluido,
					TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%i') as minutosconcluido
				from 
					chamados ch, 
					lojas l
				where 
					ch.loja_id = l.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'concluido'
					".$condicao2."
				group by 
					ch.assunto
				order by quantidadeConcluido desc, nomeLoja asc";
		 $result4 = mysql_query($query4);
		 
$query_area4  = "
				select 
					ch.area_id,
					a.nome as nomeArea,
					count(ch.area_id) as quantidadeConcluido, 
					ch.data_criacao, 
					ch.data_fechamento,
					datediff(ch.data_fechamento,ch.data_criacao) as diferencaDataConcluido,
					timediff(ch.data_fechamento,ch.data_criacao) as diferencaHotaConcluido,
					TIME_FORMAT(timediff(ch.data_fechamento,ch.data_criacao),'%k') DIV 24 as diasconcluido,
					MOD(TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%k'),24) as horasconcluido,
					TIME_FORMAT(TIMEDIFF(ch.data_fechamento,ch.data_criacao),'%i') as minutosconcluido
				from 
					chamados ch, 
					areas a
				where 
					ch.area_id = a.id 
					".$condicao." 
					and ch.data_criacao >= '$data_inicio' 
					and ch.data_criacao <= '$data_fim'
					and ch.data_fechamento <= '$data_fim' 
					and ch.status = 'concluido'
					".$condicao2."
				group by 
					ch.area_id
				order by quantidadeConcluido desc, nomeArea asc";
					  
		$result_area4 = mysql_query($query_area4);
////////////////////////////////////////////fim das querys////////////////////////////////////////////////
		 
		 
		$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'relatorio_chamados_resumido.html');	
	
		$mensagem = 'Este per&iacute;odo n&atilde;o consta com nenhum chamado nesta classifica&ccedil;&atilde;o';
		$numrows	= mysql_num_rows($result);
		$numrowsAreas = mysql_num_rows($result_area);
		
		if($numrows > 0 or $numrowsAreas > 0)
		{
			$totalChamados = 0;
			$totalabertos = 0;
			$totalAndamento = 0;
			$totalConcluido = 0;
			$totalConcluido = 0;
			$totalDataAbertos = 0;
			$totalDataAndamento = 0;
			$totalDataConcluido = 0;
			
		$campos= "";
			while ($row = mysql_fetch_array($result)) 
			{	
				$totalConcluido = 0;
				$totalDataAbertos = 0;
				$totalDataAndamento = 0;
				$totalDataConcluido = 0;
				
				$tempoAbertoDias = 0;
				$tempoAbertoHoras = 0;
				$tempoAbertoMinutos = 0;
				
				$tempoAndamentoDias = 0;
				$tempoAndamentoHoras = 0;
				$tempoAndamentoMinutos = 0;
				
				$tempoConcluidoDias = 0;
				$tempoConcluidoHoras = 0;
				$tempoConcluidoMinutos = 0;
			
				$nomeLoja				= $row['nomeAssunto'];
				$quantidadeChamados		= $row['quantidadeChamados'];
				$totalChamados += $quantidadeChamados;
				$campos	.= "<tr class='tr_txt txt_pag'>";
				$campos .= "<td class='td_txt'>".$nomeLoja."</td>";
				$campos .= "<td class='td_txt'>".$quantidadeChamados."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAbertos=0;
				///////////////////////////////////////////////////////////////
				//TIVE QUE IMPLEMENTAR DESTA FORMA POIS A BASE ESTÁ TODA ZONEADA --> BY JONATHAS E DANIEL COM AUTORIZAÇÃO DO CARLOS
				while ($row2 = mysql_fetch_array($result2)){
					if ($row['nomeAssunto']==$row2['nomeAssunto']){
						$quantidadeAbertos = $row2['quantidadeAbertos'];
						$diferencaDataAbertos	= $row2['diferencaDataAbertos'];
						$diferencaHotaAbertos	= $row2['diferencaHotaAbertos'];
						$diasAbertos			= $row2['diasAbertos'];
						$horasAbertos			= $row2['horasAbertos'];
						$minutosAbertos			= $row2['minutosAbertos'];
						$tempoAbertoDias += $diasAbertos;
						$tempoAbertoHoras += $horasAbertos;
						$tempoAbertoMinutos += $minutosAbertos;
						$totalDataAbertos += $diferencaDataAbertos;
						$totalabertos += $quantidadeAbertos;
						break;
					}
				}
				@mysql_data_seek($result2,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAbertos."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAndamento=0;
				///////////////////////////////////////////////////////////////
				while ($row3 = mysql_fetch_array($result3)){
					if ($row['nomeAssunto']==$row3['nomeAssunto']){
						$quantidadeAndamento = $row3['quantidadeAndamento'];
						$diferencaDataAndamento	= $row3['diferencaDataAndamento'];
						$diferencaHotaAndamento	= $row3['diferencaHotaAndamento'];
						$diasAndamento			= $row3['diasAndamento'];
						$horasAndamento			= $row3['horasAndamento'];
						$minutosAndamento			= $row3['minutosAndamento'];
						$tempoAndamentoDias += $diasAndamento;
						$tempoAndamentoHoras += $horasAndamento;
						$tempoAndamentoMinutos += $minutosAndamento;
						$totalDataAndamento += $diferencaDataAndamento;
						$totalAndamento += $quantidadeAndamento;
						break;
					}
				}
				@mysql_data_seek($result3,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAndamento."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeConcluido=0;
				///////////////////////////////////////////////////////////////
				while ($row4 = mysql_fetch_array($result4)){
					if ($row['nomeAssunto']==$row4['nomeAssunto']){
						$quantidadeConcluido = $row4['quantidadeConcluido'];
						$diferencaDataConcluido	= $row4['diferencaDataConcluido'];
						$diferencaHotaConcluido	= $row4['diferencaHotaConcluido'];
						$diasConcluido			= $row4['diasConcluido'];
						$horasConcluido			= $row4['horasConcluido'];
						$minutosConcluido		= $row4['minutosConcluido'];
						$tempoConcluidoDias += $diasConcluido;
						$tempoConcluidoHoras += $horasConcluido;
						$tempoConcluidoMinutos += $minutosConcluido;
						$totalDataConcluido += $diferencaDataConcluido;
						$totalConcluido += $quantidadeConcluido;
						break;
					}
				}
				@mysql_data_seek($result4,0);	
				$totalTempo = 0;
				$totalTempo = $totalDataAbertos + $totalDataAndamento + $totalDataConcluido;
				$totalTempoGeral += $totalTempo;
				
				$totalTempoDia = 0;
				$totalTempoDia = $tempoAbertoDias + $tempoAndamentoDias + $tempoConcluidoDias;
				$totalTempoDiaGeral += $totalTempoDia;
				
				$totaltempoHora = 0;
				$totaltempoHora = $tempoAbertoHoras + $tempoAndamentoHoras + $tempoConcluidoHoras;
				$totalTempoHoraGeral += $totaltempoHora;
				
				$totaltempoMinuto = 0;
				$totaltempoMinuto = $tempoAbertoMinutos + $tempoAndamentoMinutos + $tempoConcluidoMinutos;
				$totalTempoMinutoGeral += $totaltempoMinuto;
				
				$campos .= "<td class='td_txt'>".$quantidadeConcluido."</td>";
				//$campos .= "<td class='td_txt'>".$totalTempo." dia(s)</td>";
				$campos .= "<td class='td_txt'>".$totalTempoDia." dia(s) ".$totaltempoHora." hora(s) ".$totaltempoMinuto." minuto(s)</td>";
				$campos .= "</tr>";
				
			}
			////////////////////areas//////////////////////////////////////////
				while ($row_area = mysql_fetch_array($result_area)) 
			{		
				$totalConcluido = 0;
				$totalDataAbertos = 0;
				$totalDataAndamento = 0;
				$totalDataConcluido = 0;
				
				$tempoAbertoDias = 0;
				$tempoAbertoHoras = 0;
				$tempoAbertoMinutos = 0;
				
				$tempoAndamentoDias = 0;
				$tempoAndamentoHoras = 0;
				$tempoAndamentoMinutos = 0;
				
				$tempoConcluidoDias = 0;
				$tempoConcluidoHoras = 0;
				$tempoConcluidoMinutos = 0;
			
				$nomeLoja				= $row_area['nomeArea'];
				$quantidadeChamados		= $row_area['quantidadeChamados'];
				$totalChamados += $quantidadeChamados;
				$campos	.= "<tr class='tr_txt txt_pag'>";
				$campos .= "<td class='td_txt'>".$nomeLoja."</td>";
				$campos .= "<td class='td_txt'>".$quantidadeChamados."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAbertos=0;
				///////////////////////////////////////////////////////////////
				//TIVE QUE IMPLEMENTAR DESTA FORMA POIS A BASE ESTÁ TODA ZONEADA --> BY JONATHAS E DANIEL COM AUTORIZAÇÃO DO CARLOS
				while ($row_area2 = mysql_fetch_array($result_area2)){
					if ($row_area['area_id']==$row_area2['area_id']){
						$quantidadeAbertos = $row_area2['quantidadeAbertos'];
						$diferencaDataAbertos	= $row_area2['diferencaDataAbertos'];
						$diferencaHotaAbertos	= $row_area2['diferencaHotaAbertos'];
						$diasAbertos			= $row_area2['diasAbertos'];
						$horasAbertos			= $row_area2['horasAbertos'];
						$minutosAbertos			= $row_area2['minutosAbertos'];
						$tempoAbertoDias += $diasAbertos;
						$tempoAbertoHoras += $horasAbertos;
						$tempoAbertoMinutos += $minutosAbertos;
						$totalDataAbertos += $diferencaDataAbertos;
						$totalabertos += $quantidadeAbertos;
						break;
					}
				}
				@mysql_data_seek($result_area2,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAbertos."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeAndamento=0;
				///////////////////////////////////////////////////////////////
				while ($row_area3 = mysql_fetch_array($result_area3)){
					if ($row_area['area_id']==$row_area3['area_id']){
						$quantidadeAndamento = $row_area3['quantidadeAndamento'];
						$diferencaDataAndamento	= $row_area3['diferencaDataAndamento'];
						$diferencaHotaAndamento	= $row_area3['diferencaHotaAndamento'];
						$diasAndamento			= $row_area3['diasAndamento'];
						$horasAndamento			= $row_area3['horasAndamento'];
						$minutosAndamento		= $row_area3['minutosAndamento'];
						$tempoAndamentoDias += $diasAndamento;
						$tempoAndamentoHoras += $horasAndamento;
						$tempoAndamentoMinutos += $minutosAndamento;
						$totalDataAndamento += $diferencaDataAndamento;
						$totalAndamento += $quantidadeAndamento;
						break;
					}
				}
				@mysql_data_seek($result_area3,0);				
				$campos .= "<td class='td_txt'>".$quantidadeAndamento."</td>";
				////////////representa��o da vari�vel quando for nula///////////
				$quantidadeConcluido=0;
				///////////////////////////////////////////////////////////////
				while ($row_area4 = mysql_fetch_array($result_area4)){
					if ($row_area['area_id']==$row_area4['area_id']){
						$quantidadeConcluido = $row_area4['quantidadeConcluido'];
						$diferencaDataConcluido	= $row_area4['diferencaDataConcluido'];
						$diferencaHotaConcluido	= $row_area4['diferencaHotaConcluido'];
						$diasConcluido			= $row_area4['diasConcluido'];
						$horasConcluido			= $row_area4['horasConcluido'];
						$minutosConcluido		= $row_area4['minutosConcluido'];
						$tempoConcluidoDias += $diasConcluido;
						$tempoConcluidoHoras += $horasConcluido;
						$tempoConcluidoMinutos += $minutosConcluido;
						$totalDataConcluido += $diferencaDataConcluido;
						$totalConcluido += $quantidadeConcluido;
						break;
					}
				}
				@mysql_data_seek($result_area4,0);
				$totalTempo = 0;
				$totalTempo = $totalDataAbertos + $totalDataAndamento + $totalDataConcluido;
				$totalTempoGeral += $totalTempo;
				
				$totalTempoDia = 0;
				$totalTempoDia = $tempoAbertoDias + $tempoAndamentoDias + $tempoConcluidoDias;
				$totalTempoDiaGeral += $totalTempoDia;
				
				$totaltempoHora = 0;
				$totaltempoHora = $tempoAbertoHoras + $tempoAndamentoHoras + $tempoConcluidoHoras;
				$totalTempoHoraGeral += $totaltempoHora;
				
				$totaltempoMinuto = 0; 
				$totaltempoMinuto = $tempoAbertoMinutos + $tempoAndamentoMinutos + $tempoConcluidoMinutos;
				$totalTempoMinutoGeral += $totaltempoMinuto;
				
				$campos .= "<td class='td_txt'>".$quantidadeConcluido."</td>";
				//$campos .= "<td class='td_txt'>".$totalTempo." dia(s)</td>";
				$campos .= "<td class='td_txt'>".$totalTempoDia." dia(s) ".$totaltempoHora." hora(s) ".$totaltempoMinuto." minuto(s)</td>";
				$campos .= "</tr>";
				
			}
			$campoTotal .= "<tr class='tr_tit txt_tit'><td class='td_txt' align='left' colspan='6'>Total Geral</td></tr>";
			$campoTotal .= "<tr class='tr_txt txt_pag'>		
	   								<td class='td_txt'><b>Total:</b></td>
									<td class='td_txt'>".$totalChamados."</td>
									<td class='td_txt'>".$totalabertos."</td>
									<td class='td_txt'>".$totalAndamento."</td>
									<td class='td_txt'>".$totalConcluido."</td>
									<td class='td_txt'>".$totalTempoDiaGeral." dia(s) ".$totalTempoHoraGeral." hora(s) ".$totalTempoMinutoGeral." minuto(s)</td>
								</tr>";
			//}
		}
		else
		{
			$campos 		.= "<tr class='tr_txt txt_pag'>
									<td class='td_txt' colspan='6'>".$mensagem."</td>
									
									
								</tr> ";
		}
		
		$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
		$conteudo_tpl		= str_replace ('##CAMPOMEDIA##', $campoTotal ,$conteudo_tpl);
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		////////////////////////////////////////////////////////////////
		
	}
		
	include("../navegacao.php");
	
	include("busca_relatorio_chamados_resumido.php");
	
	?>
		<div class="conteudo">
        	
        	<div id="pag_1" style="text-align:right;padding-right:20px;padding-top:10px; margin-bottom:1px;">
				<? echo $paginacao; ?>
            </div>
	<?
			
			echo $conteudo_tpl;
			
	?>
	   		<div id='pag_2' style="text-align:right;padding-right:20px;padding-top:5px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>