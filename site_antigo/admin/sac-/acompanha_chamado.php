<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_acompanhar_chamado", $_SESSION['id_usuario']) and verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_sac", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	
	$submit = $_POST['submit'];
	
	if (isset($submit))
	{
		$chamado_id = $_POST['chamado_id'];
		$cliente_id = $_POST['cliente_id'];//id do cliente que abriu o chamado
		$resposta	= $_POST['comentario'];
		$id_status	= $_POST['status'];
		$visivel 	= $_POST['privado'];
		
		// alterando o status \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$query_status = "select * from chamados_status where id = $id_status";
		$result_status= mysql_query($query_status);
		$row_status = mysql_fetch_array($result_status);
		
		if($row_status['status'] == "concluido"){
			$data_fechamento = dataAtual();
		}else{
			$data_fechamento = 0;
		}
		
		$update_status = "update chamados set status = '".$row_status['status']."', data_fechamento = '$data_fechamento' where id = $chamado_id ";
		
		$result_update_status= mysql_query($update_status);
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\		
		if($visivel == 1){
			$visivel = 0;// significa que foi marcado como privado
		}else{
			$visivel = 1;// disponivel para o cliente ver
		}
		
		// se a nao for preenchida a resposta, nao grava
		if($resposta != "")
		{
			$data_nova_resposta	= dataAtual();
			
			$query_user = "select * from usuarios where id = ".$_SESSION['id_usuario']." ";
			$result_user= mysql_query($query_user);
			$row_user = mysql_fetch_array($result_user);
			$id_user  = $row_user['id'];
					
			$query = "INSERT INTO respostas VALUES ('','$chamado_id', '$id_user', '$resposta', '$data_nova_resposta', '$visivel' )";
			$result = mysql_query ($query);
			
			if($result)
			{
				alert('Resposta adicionada com sucesso.');
				redirect("acompanha_chamado.php?id_chamado=$chamado_id");		
			}
			else
			{
				alert('Erro ao criar chamado.');
				redirect('lista_chamados.php');
			}	
		}//fim do if($resposta)
		else
		{
			redirect("acompanha_chamado.php?id_chamado=$chamado_id");
		}
		
	}
	else////////////////////////////////////////////////////////////////////////////////////
	{
		$id_chamado			= $_GET['id_chamado'];
		
		$conteudo_tpl 		= AbrePag(DIR_TEMPLATES.'acompanha_chamado.html');
		$conteudo_tpl 		= str_replace ('##ACTION##', 'acompanha_chamado.php', $conteudo_tpl);
		
		$query = "select * from chamados where id = $id_chamado";
		$result = mysql_query($query);
		
		$row = mysql_fetch_array($result);
		$status = $row['status'];
		
		// pegando o status para exibir acima da tela de historico\\\\\\\
		if($status == "concluido"){
			$data = saidaData($row['data_fechamento']);
			$hora = saidaHora($row['data_fechamento']);
			
			$fechamento = "<b>Concluido em</b>: ".$data. " �s ". $hora;
			$conteudo_tpl = str_replace("##FECHAMENTO##", $fechamento , $conteudo_tpl);
			$conteudo_tpl = str_replace("##ABERTURA##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##DISABLED##", 'disabled=disabled' , $conteudo_tpl);
		}
		elseif($status == "aberto")
		{
			$data = saidaData($row['data_criacao']);
			$hora = saidaHora($row['data_criacao']);
			
			$abertura = "<b>Aberto em</b>: ".$data. " �s ". $hora;
			$conteudo_tpl = str_replace("##ABERTURA##", $abertura , $conteudo_tpl);
			$conteudo_tpl = str_replace("##FECHAMENTO##", '' , $conteudo_tpl);
		}
		else
		{
			$data = saidaData($row['data_criacao']);
			$hora = saidaHora($row['data_criacao']);
			
			$abertura = "<b>Aberto em</b>: ".$data. " �s ". $hora;
			$conteudo_tpl = str_replace("##ABERTURA##", $abertura , $conteudo_tpl);
			$conteudo_tpl = str_replace("##FECHAMENTO##", '' , $conteudo_tpl);
		}
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		if($row['area_id'] > 0)
		{
			$query_area = "select * from areas where id = ".$row['area_id']." ";
			$result_area= mysql_query($query_area);
			$row_area = mysql_fetch_array($result_area);
			$conteudo_tpl  = str_replace("##TIT_AREAS##", '�rea: ' , $conteudo_tpl);
			$conteudo_tpl  = str_replace("##AREAS##", $row_area['nome'], $conteudo_tpl);	
		}
		else{
			$conteudo_tpl  = str_replace("##TIT_AREAS##", '' , $conteudo_tpl);
			$conteudo_tpl  = str_replace("##AREAS##", '' , $conteudo_tpl);	
		}
		if($row['loja_id'] > 0)
		{
			$query_loja = "select * from lojas where id = ".$row['loja_id']." ";
			$result_loja= mysql_query($query_loja);
			$row_loja = mysql_fetch_array($result_loja);
			$conteudo_tpl  = str_replace("##TIT_LOJAS##", 'Loja: ' , $conteudo_tpl);
			$conteudo_tpl  = str_replace("##LOJAS##", $row_loja['nome'], $conteudo_tpl);	
		}
		else
		{
			$conteudo_tpl  = str_replace("##TIT_LOJAS##", '' , $conteudo_tpl);
			$conteudo_tpl  = str_replace("##LOJAS##", '' , $conteudo_tpl);	
		}
					
		/////////////////// hiddens \\\\\\\\\\\\\\\\\\\\
		$hidden_chamado_id 		= "<input name = 'chamado_id' type = 'hidden' value = '".$row['id']."' />";
		$conteudo_tpl 	   = str_replace("##HIDDEN_CHAMADO##", $hidden_chamado_id  , $conteudo_tpl);
		
		$hidden_cliente_id 	= "<input name = 'cliente_id' type = 'hidden' value = '".$row['usuario_id']."' />";
		$conteudo_tpl 	   = str_replace("##HIDDEN_CLIENTE_ID##", $hidden_cliente_id  , $conteudo_tpl);
		/////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////buscando o status anterior////////////////////////////
		$query_status = "select * from chamados_status where status = '".$row['status']."' ";
		$result_status= mysql_query($query_status);
		$row_status = mysql_fetch_array($result_status);
		$id_status = $row_status['id'];
		$status = $row_status['status'];
		if($status == "aberto")$id_status = 2;
		////////////////////////////////////////////////////////////////////////////////
		$FazCombo_status   = FazCombo('status', 'status', 'chamados_status', $id_status);
		$conteudo_tpl = str_replace("##STATUS##", $FazCombo_status, $conteudo_tpl);	
		
		
		if($row['assunto'] != "")
		{
			$conteudo_tpl = str_replace("##TIT_ASSUNTO##", '<tr><td><b>Assunto: </b></td>' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##ASSUNTO##", "<td style='padding-left:10px;'>".$row['assunto']."</td></tr>" , $conteudo_tpl);
		}
		else
		{
			$conteudo_tpl = str_replace("##TIT_ASSUNTO##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##ASSUNTO##", '' , $conteudo_tpl);
		}
		if($row['nota_fiscal'] != "")
		{
			$conteudo_tpl = str_replace("##TIT_NOTA_FISCAL##", '<tr><td><b>Nota Fiscal: </b></td>' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##NOTA_FISCAL##", "<td style='padding-left:10px;'>".$row['nota_fiscal']."</td></tr>" , $conteudo_tpl);
		}
		else
		{
			$conteudo_tpl = str_replace("##TIT_NOTA_FISCAL##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##NOTA_FISCAL##", '' , $conteudo_tpl);
		}
		if($row['nome_funcionario'] != "")
		{
			$conteudo_tpl = str_replace("##TIT_FUNCIONARIO##", '<tr><td><b>Nome do Funcion�rio: </b></td>' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##NOME_FUNCIONARIO##", "<td style='padding-left:10px;'>".$row['nome_funcionario']."</td></tr>" , $conteudo_tpl);
		}
		else
		{
			$conteudo_tpl = str_replace("##TIT_FUNCIONARIO##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##NOME_FUNCIONARIO##", '' , $conteudo_tpl);
		}
		if($row['descricao'] != "")
		{
			$conteudo_tpl = str_replace("##TIT_DESCRICAO##", '<tr><td><b>Descri��o: </b></td>' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##DESCRICAO##", "<td style='padding-left:10px;'>".$row['descricao']."</td></tr>" , $conteudo_tpl);
			
		}
		else
		{
			$conteudo_tpl = str_replace("##TIT_DESCRICAO##", '' , $conteudo_tpl);
			$conteudo_tpl = str_replace("##DESCRICAO##", '' , $conteudo_tpl);
		}
			
			$conteudo_tpl = str_replace("##CODIGO_CHAMADO##", $id_chamado , $conteudo_tpl);
			
			
		/////////// buscando as respostas daquele usuario e o chamado \\\\\\\\\\
		$query_respostas = "select * from respostas where chamado_id = $id_chamado order by data_criacao asc ";
		$result_respostas= mysql_query($query_respostas);
		
		$logs 	.= "<table class='tabela_listagem' align='left' cellpadding='0' cellspacing='2'>";
		
		
		while($row_respostas = mysql_fetch_array($result_respostas))
		{
			$id_chamado		= $row_respostas['chamado_id'];
			$id_cliente		= $row_respostas['usuario_id'];
			$resposta		= $row_respostas['resposta'];
			$data_criacao 	= $row_respostas['data_criacao'];
			$visivel		= $row_respostas['visivel'];
			
			if($visivel == 0) $visivel = "(privado)";
			else $visivel = '';
		
			
			$data = saidaData($data_criacao);
			$hora = saidaHora($data_criacao);
			
			$data_criacao = $data. " �s ". $hora;
			
			//verificando se quem respondeu foi o admin
			$query_user = "select * from usuarios where id = ".$id_cliente." ";
			$result_user= mysql_query($query_user);
			$num_rows = mysql_num_rows($result_user);
			if($num_rows > 0){
				$row_user = mysql_fetch_array($result_user);
				$id_user  = $row_user['id'];
				$nome  = $row_user['login'];
			}else{
				$query_cliente = "select * from clientes where id = $id_cliente";
				$result_cliente= mysql_query($query_cliente);
				$row_cliente   = mysql_fetch_array($result_cliente);
				$nome 		   = $row_cliente['nome_completo'];	
			}								
										
			$logs 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt'>
									$data_criacao &nbsp;&nbsp;&nbsp; <b>por: </b>$nome
								</td>
								
							</tr>";
			$logs 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt' align='left'>
									<b>$visivel</b> <i> $resposta </i>
								</td>
								
							</tr>
							<tr>
								<td><hr></td>
							</tr>";
			
			$i++;
		}

		$logs 		.= "</table>";
		
		$link_encaminhar_chamado ="".DIR_ABS."admin/sac/encaminha_chamado.php?id_chamado=$id_chamado";
		$conteudo_tpl 		= str_replace ('##LINK##', $link_encaminhar_chamado ,$conteudo_tpl);
		
		$conteudo_tpl 		= str_replace ('##LOGS##', $logs ,$conteudo_tpl);
		
		
		////////////////////////////////////////////////////////////////
		include("../navegacao.php");
	?>
		<div class="conteudo">
	<?
		echo $conteudo_tpl;
	?>
		</div> 
	<?
	
	////////////////////////////////////////////////////////////////
	}

}// fim da permissao
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>