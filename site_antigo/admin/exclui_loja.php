<? include("topo.php");
////////////////////////////////////////
include("../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$id_loja = $_GET['id_loja'];
	$acao = $_GET['acao'];

	$query = "SELECT * FROM lojas WHERE id = ".$id_loja;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	
	$nome = $row['nome'];
	$foto = $row['foto'];
	$logo = $row['logomarca'];
	
	if($acao == 'excluir_foto')
	{
		$mini = explode(".", $foto);
		$foto_mini = $mini[0]."_mini".".".$mini[1];
		@unlink("../images/lojas/fachadas/".$foto);
		@unlink("../images/lojas/fachadas/".$foto_mini);
		
		$query_foto = "update lojas set foto = '' where id = $id_loja";
		$result_foto= mysql_query($query_foto);
		if($result_foto)
		{
			alert('Foto excluida com sucesso.');
			die(redirect("edita_loja.php?id_loja=$id_loja"));
		}
	}
	if($acao == 'excluir_logo')
	{
		$mini = explode(".", $logo);
		$logo_mini = $mini[0]."_mini".".".$mini[1];
		@unlink("../images/lojas/logos/".$logo);
		@unlink("../images/lojas/logos/".$logo_mini);
		
		$query_logo = "update lojas set logomarca = '' where id = $id_loja";
		$result_logo= mysql_query($query_logo);
		if($result_logo)
		{
			alert('Logomarca excluida com sucesso.');
			die(redirect("edita_loja.php?id_loja=$id_loja"));
		}
	}
	
	$query_del = "DELETE FROM lojas WHERE id = '".$row['id']."' ";
	$result_del = mysql_query($query_del);
	
	if($result_del)
	{
		if( $foto != "" )
		{
			$mini = explode(".", $foto);
			$foto_mini = $mini[0]."_mini".".".$mini[1];
			@unlink("../images/lojas/fachadas/".$foto);
			@unlink("../images/lojas/fachadas/".$foto_mini);
		}
		if( $logo != "" )
		{
			$mini = explode(".", $logo);
			$logo_mini = $mini[0]."_mini".".".$mini[1];
			@unlink("../images/lojas/logos/".$logo);
			@unlink("../images/lojas/logos/".$logo_mini);
		}
		$query_del_catSub = "DELETE FROM subcategorias_lojas WHERE id_loja = '".$row['id']."' ";
		$result_del_catSub= mysql_query($query_del_catSub);
		
		alert('Loja excluida com sucesso.');
		redirect("gerencia_lojas.php");
	}
	else
	{
		alert('Erro ao excluir loja.');
		redirect("gerencia_lojas.php");
		//header("Location: index.php");
	}
	
	////////////////////////////////////////////////////////////////
		include("navegacao.php");
	?>
		<div class="conteudo">
	<?
		echo $conteudo_tpl;
	?>
		</div> 
	<?
	include("rodape.php");
		////////////////////////////////////////////////////////////////
		
}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
?>
