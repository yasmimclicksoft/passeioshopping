<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_cadastrar_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	$submit = $_POST['submit'];
	
	if(isset($submit))
	{
		$id_post = $_POST['id_post'];
		$titulo  = $_POST['titulo'];
		$texto 	 = $_POST['texto'];
		
		if($titulo == "" and $texto == "" )
		{
			alert('Preencha ao menos um dos campos.');
			redirect("gerencia_posts.php");
			die();
		}
		if($_FILES['foto']['name'] != "")
		{
			$nome_foto 	= basename($_FILES['foto']['name']);
			$ext 		= explode('.', $nome_foto);
			$extensao	= strtolower($ext[1]);
			
			if($extensao != "jpg")
			{
				alert('Apenas fotos de extens�o jpg s�o aceitas.A foto n�o p�de ser cadastrada.');
				redirect("gerencia_posts.php");	
			}
			else
			{
				$path 		= "../../images/blog/".$id_post.".".$extensao;
				$foto		= $id_post.".".$extensao;
				
				if (move_uploaded_file($_FILES['foto']['tmp_name'], $path )) 
				{
					thumbit ($path,156,117,S);
					thumbit ($path,300,375,N);
					
					$query 	= " update blog_posts set titulo = '$titulo', texto = '$texto', foto = '$foto' where id = '$id_post' ";
					$result = mysql_query($query);
				}
			}// fim do else extensao
		}
		else
		{				
			$query	="update blog_posts set titulo='$titulo', texto='$texto' where id=$id_post";
			$result = mysql_query($query);
		}
		if($result)
		{
			alert('Post alterado com sucesso.');
			redirect("gerencia_posts.php");	
		 }
		 else
		 {
		 	alert('Erro ao alterar o post.');
			redirect("gerencia_posts.php");	
		 }
	}
	else ////////////////// abre a pagina \\\\\\\\\\\\\\\
	{
		$conteudo_tpl 	= AbrePag(DIR_TEMPLATES.'editar_post.html');
		$conteudo_tpl 	= str_replace ('##ACTION##', 'editar_post.php', $conteudo_tpl);
		
		$id_post = $_GET['id_post'];
		
		$query	= "select * from blog_posts where id = $id_post";
		$result	= mysql_query($query);
		$row = mysql_fetch_array($result);
		
		$titulo = $row['titulo'];
		$texto 	= $row['texto'];
		
		$submit_id 			 = "<input name= 'id_post' type= 'hidden' value ='".$id_post."' />";
		$conteudo_tpl		 = str_replace ("##HIDDEN##" , $submit_id , $conteudo_tpl);
		$conteudo_tpl		 = str_replace ("##TITULO##" , $titulo , $conteudo_tpl);
		$conteudo_tpl		 = str_replace ("##TEXTO##" , $texto , $conteudo_tpl);
		
		if($row['foto'] > 0)
		{
			$foto = explode(".",$row['foto']);
			$foto_mini = $foto[0]."_mini.".$foto[1];
			$foto_mini = "<img src='../../images/blog/$foto_mini' border='1' />";
			$conteudo_tpl = str_replace("##FOTO##", $foto_mini , $conteudo_tpl);
			$acao="<a onclick=\"confirma('Tem certeza que deseja excluir esta foto ?', 'excluir_post.php?acao=foto&id_post=$id_post');\" href='#'>excluir foto</a>";
			$conteudo_tpl = str_replace("##EXCLUIR_FOTO##", $acao , $conteudo_tpl);
		}
		else
		{
			$foto = "<img src='../../images/sem_foto.jpg ' border='1' />";
			$conteudo_tpl 	 = str_replace("##FOTO##", $foto , $conteudo_tpl);
			$conteudo_tpl 	 = str_replace("##EXCLUIR_FOTO##", 'sem foto' , $conteudo_tpl);
		}
		////////////////////////////////////////////////////////////////
		
		include("../navegacao.php");
		?>
		<div class="conteudo">
		<?
			echo $conteudo_tpl;
		?>
		</div> 
		<?
	}////////////////////////////////////////////////////////////////
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	require_once("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
