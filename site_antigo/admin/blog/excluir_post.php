<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_exluir_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	$id_post = $_GET['id_post'];
	$acao = $_GET['acao'];
	
	if($acao == 'foto')
	{
		$query 	= "select foto from blog_posts where id = $id_post";
		$result	= mysql_query($query);
		
		if($result)
		{
			$row = mysql_fetch_array($result);
			$foto = $row['foto'];
			
			$mini = explode(".", $foto);
			$foto_mini = $mini[0]."_mini".".".$mini[1];
			@unlink("../../images/blog/".$foto);
			@unlink("../../images/blog/".$foto_mini);
		
			$query_foto = "update blog_posts set foto = '' where id = $id_post";
			$result_foto= mysql_query($query_foto);
		
			if($result_foto)
			{
				alert('Foto excluida com sucesso.');
				die(redirect("editar_post.php?id_post=$id_post"));
			}
		}
	}
	else
	{
		$query 	= "select foto from blog_posts where id = $id_post";
		$result	= mysql_query($query);
		
		if($result)
		{
			$row = mysql_fetch_array($result);
			$foto = $row['foto'];
						
			$query_del = "delete from blog_posts where id = '$id_post' ";
			$result_del = mysql_query($query_del);
		
			if($result_del)
			{
				if( $foto != "" )
				{
					$mini = explode(".", $foto);
					$foto_mini = $mini[0]."_mini".".".$mini[1];
					@unlink("../../images/blog/".$foto);
					@unlink("../../images/blog/".$foto_mini);
				}
				$query_comentarios	= "select id from blog_posts_comentarios where post_id = $id_post";
				$result_comentarios	= mysql_query($query_comentarios);
				
				if($result_comentarios)
				{
					$query_del_comentarios	= "delete from blog_posts_comentarios where post_id = $id_post";
					$result_del_comentarios	= mysql_query($query_del_comentarios);
				}
				alert('Post excluido com sucesso.');
				redirect("gerencia_posts.php");
			}
			else
			{
				alert('Erro ao excluir post.');
				redirect("gerencia_posts.php");
			}
		}
	}
	
	////////////////////////////////////////////////////////////////
	include("../navegacao.php");
	
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
