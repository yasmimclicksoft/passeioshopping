<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_geral_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\
	$order = "data desc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
	
	//pegando o id do post enviado pelo gerenciar post para listar seus comentarios
	$id_post = $_GET['id_post'];	
	
	// pega o numero total de registros
	$query = "SELECT count(id) as total from blog_posts_comentarios where post_id = $id_post";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from blog_posts_comentarios where post_id = $id_post order by $order limit $inicio, $limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'lista_comentarios.html');
	
	$i = 0;
	while ($row = mysql_fetch_array($result)) 
	{
		$id_comentario= $row['id'];
		$id_cliente	  = $row['cliente_id'];
		$data		  = saidaData($row['data']);
		$comentario	  = $row['comentario'];
		
		
		//pegando o usuario que comentou o post (id do usuario da tabela usuarios e nao o user_id)
		$query_user	  = "select login from usuarios where id = ".$id_cliente." ";
		$result_user  = mysql_query($query_user);
		$usuario	  = mysql_result($result_user, 0 , 'login');
		
		$acao_excluir = "<a onclick=\"confirma('Deseja excluir este comentário?', 'excluir_comentario.php?id_comentario=$id_comentario&id_post=$id_post');\" href='#'><img src='../../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
					
		$campos 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt'>$usuario</td>
								<td class='td_txt'>$data</td>
								<td class='td_txt'>$comentario</td>
								<td class='td_txt' align='center' >$acao_excluir</td>
							</tr>";
		$i++;
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	////////////////////////////////////////////////////////////////////////////	
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"lista_comentarios.php?id_post=$id_post&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"lista_comentarios.php?id_post=$id_post&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"lista_comentarios.php?id_post=$id_post&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
			
	////////////////////////////////////////////////////////////////
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("../navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?


}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
