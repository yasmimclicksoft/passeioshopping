<? include("topo.php"); 
////////////////////////////////////////
include("../includes/functions.php");
conexao();
////////////////////////////////////////

$id_user = $_GET['id_user'];
//echo $_SESSION['id_usuario'];
if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\\\
	$order = "nome asc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros para paginacao
	$query = "SELECT count(id) as total from eventos";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from filmes order by $order limit $inicio,$limite_pagina";
	$result = mysql_query($query);
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_filmes.html');
	
	while ($row = mysql_fetch_array($result)) 
	{
		$id_filme		= $row['id'];
		$nome 			= $row['nome'];
		$sinopse		= $row['sinopse'];
		$genero 		= $row['genero'];
		$etaria 		= $row['classificacao_etaria'];
		$data_inicio	= saidaData($row['data_inicio']);
		$data_fim		= saidaData($row['data_fim']);
		
		$abrev =  substr($sinopse , 0 , 45); 
		if(strlen($sinopse) > 45) $abrev = $abrev. "...";
		
		$acao_editar	= "<a href='edita_filme.php?id_filme=$id_filme'><img src='../imagens_layout/btn_editar_p.jpg' border=0 /></a>";
		$acao_excluir	= "<a onclick=\"confirma('Tem certeza que deseja excluir o filme ".$nome."?', 'exclui_filme.php?id_filme=$id_filme');\" href='#'><img src='../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
			
		$campos 		.= "<tr class='tr_txt txt_pag' >
								<td class='td_txt' >$nome</td>
								<td class='td_txt'>$abrev</td>
								<td class='td_txt'>$genero</td>
								<td class='td_txt'>$etaria</td>
								<td class='td_txt'>$data_inicio</td>
								<td class='td_txt'>$data_fim</td>
								<td class='td_txt' align=center nowrap='nowrap'>$acao_editar &nbsp; $acao_excluir</td>
							</tr> ";
		
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	///////////// paginacao \\\\\\\\\\\\\\\\\\
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_filmes.php?nome=$nome&order=nome asc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_filmes.php?nome=$nome&order=nome asc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_filmes.php?nome=$nome&order=nome asc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
	
	////////////////////////////////////////////////////////////////
	
	include("navegacao.php");
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	?>
		<div class="conteudo">
        
	<?
			echo $conteudo_tpl;
			//echo $paginacao;
	?>
	   		<div style="text-align:right;padding-right:20px;"><? echo $paginacao; ?></div>
        </div>
	<?

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
	include("rodape.php");
	////////////////////////////////////////////////////////////////
?>