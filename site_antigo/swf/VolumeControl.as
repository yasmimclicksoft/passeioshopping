﻿//classe do controle do volume, trabalharemos como se fosse uma scroll de texto na horizontal...
//
dynamic class VolumeControl extends MovieClip {
	
	//declarando variáveis utilizadas dentro do mc...
	private var btVolume:MovieClip;
	private var mcSeekVolume:MovieClip;
	private var somVideo:Sound;
	
	function VolumeControl(){
	}
	
	//função que passa ao mcVolumeControl onde achar o objeto do volume...
	public function setVolumeObject(som:Sound):Void
	{
		somVideo = som;
	}
	
	//função responsável por atualizar o volume...
	public function atualizaVolume():Void
	{
		//calculando volume...
		var v:Number = Math.round((btVolume._x / mcSeekVolume._width) * 100);
		//por fim setando o volume no objeto som de nosso player...
		somVideo.setVolume(v);
	}
	
	//função de inicio do mc, assim ja declara as ações de cada objeto...
	private function onLoad():Void
	{
		//posicionando o bt arrastavel no final da bar ou em 100% de volume...
		btVolume._x = mcSeekVolume._width;
		//adicionando ação de drag ao botão...
		btVolume.onPress = function():Void
		{
			//definindo ação de drag (arrastar) ao botão, onde a largura do mcSeekVolume._width e a area que ele possui a ser arrastável...
			this.startDrag(false, 0, 0, (this._parent.mcSeekVolume._width), 0);
			//ação monitor de setar o volume a medida que a barrinha e mexida...
			//esta classe mx.utils.Delegate e um utilitário para controle de funções em outros escopos, ou seja eu de dentro do botão ativo a função na classe...
			this.onMouseMove = mx.utils.Delegate.create(this._parent, this._parent.atualizaVolume);
			//parando monitor de volume e drag do botão...
			this.onRelease = this.onReleaseOutside = function():Void
			{
				this.stopDrag();
				delete this.onMouseMove;
			}
		}
	}
	
}