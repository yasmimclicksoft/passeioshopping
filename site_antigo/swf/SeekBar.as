﻿import mx.utils.Delegate;

class SeekBar extends MovieClip {

	public var mcSeekFundo:MovieClip;
	public var mcProgresso:MovieClip;
	public var btSeekBar:MovieClip;
	public var totalTime:Number = 0;
	private var timerProgress:Number;
	private var timerPlay:Number;
	private var objVideo:NetStream;
	
	function SeekBar(){
	}
	
	public function setVideoStream(v:NetStream):Void
	{
		objVideo = v;
		objVideo.bt = btSeekBar;
		objVideo.onMetaData = function(info:Object):Void
		{
			this.totalTime = info.duration;
			this.bt._visible = true;
		}
	}
	
	private function updateProgress():Void
	{
		var bl:Number = objVideo.bytesLoaded;
		var bt:Number = objVideo.bytesTotal;
		var p:Number = Math.round((bl/bt) * 100);
		
		mcProgresso._xscale = p;

		if(p >= 100){
			clearInterval(timerProgress);
		}
		
	}
	
	private function updateTime():Void
	{
		var p:Number = Math.max(Math.round((btSeekBar._x/mcProgresso._width) * 100), 0);
		var nT:Number = (p/100) * objVideo.totalTime;
		
		objVideo.seek(nT);
		objVideo.pause(true);
		
	}
	
	private function setTimer():Void
	{
		updatePlay();
		objVideo.pause(false);
		timerPlay = setInterval(Delegate.create(this, updatePlay), 10);
	}
	
	private function dragBar():Void
	{
		if(objVideo.totalTime > 0){
			clearInterval(timerPlay);
			btSeekBar.startDrag(false, 0, 0, (mcProgresso._width-btSeekBar._width), 0);
			btSeekBar.onMouseMove = Delegate.create(this, updateTime);
			btSeekBar.setTimer = Delegate.create(this, setTimer);
			btSeekBar.onRelease = btSeekBar.onReleaseOutside = function():Void
			{
				this.stopDrag();
				this.onMouseMove();
				this.setTimer();
				delete this.onMouseMove;
			}
		}
	}
	
	private function updatePlay():Void
	{
		var p:Number = Math.max(Math.round((objVideo.time/objVideo.totalTime) * 100), 0);
		var nX:Number = (p/100) * mcSeekFundo._width;
		
		btSeekBar._x = nX;
		
	}
	
	private function onLoad():Void
	{
		btSeekBar._x = 0;
		mcProgresso._x = 0;
		mcSeekFundo._x = 0;
		mcProgresso._xscale = 0;
		btSeekBar._visible = false;
		
		timerProgress = setInterval(Delegate.create(this, updateProgress), 20);
		setTimer();
		
		btSeekBar.onPress = Delegate.create(this, dragBar);
		
	}
	
}