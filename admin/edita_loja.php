<? include("topo.php");
////////////////////////////////////////
include("../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
}
if($permissao){

	$id_loja = $_GET['id_loja'];
	$submit = $_POST['submit'];

if (isset($submit))
{
	$id_loja 		= $_POST['id_loja'];
	// pega variaveis
	$nome 			 = trim($_POST['nome']);
	$localizacao 	 = $_POST['localizacao'];
	$telefone 		 = $_POST['telefone'];
	$email 			 = trim($_POST['email']);
	$email			 = strtolower($email);//convertendo o email para minusculo
	$site 			 = trim($_POST['site']);
	$site			 = strtolower($site);//convertendo o site para minusculo
	$palavras_chave = $_POST['palavras_chave'];
	$id_categoria 	 = $_POST['categorias'];
	$id_subcategoria = $_POST['subcategorias'];
	
	$subcategorias = array();
	//pegando tudo que eh enviado por post e separando as subcategorias para gravar	
	foreach ($_POST as $key => $value) 
	{
    	//echo "Chave: $key; Valor: $value<br />\n";
		if($key == "selecionados"){
			foreach ($value as $value_subcategorias)
				$subcategorias[]=$value_subcategorias;
				//echo gettype ($value_subcategorias+0)."<br>";
		}
	}
	/*echo "<pre>";
	print_r($subcategorias);
	echo "</pre><br>";
	echo count($subcategorias);
	die();*/
	if($_FILES['foto']['name'] != "")
	{
		$nome_imagem 	= basename($_FILES['foto']['name']);
		$ext 			= explode('.', $nome_imagem);
		$extensao		= strtolower($ext[1]);
		
		/*if($extensao != "jpeg" and $extensao != "jpg" and $extensao != "png" and $extensao != "gif")
		{
			alert('Apenas as imagens dos tipos: jpeg, jpg, png e gif, s�o aceitas. A imagem n�o p�de ser cadastrada.');
			redirect("cadastro_loja.php");	
		}*/
		if($extensao != "jpg")
		{
			alert('Apenas imagens de extens�o jpg s�o aceitas. A imagem n�o p�de ser cadastrada.');
			redirect("cadastro_loja.php");	
		}
		else
		{
			$path 		= "../images/lojas/fachadas/".$id_loja.".".$extensao;
			$foto		= $id_loja.".".$extensao;
			
			if (move_uploaded_file($_FILES['foto']['tmp_name'], $path )) 
			{
				//thumbit ($path,156,117,S);
				//thumbit ($path,500,375,N);
				thumbit ($path,156,117,S);
				thumbit ($path,640,480,N);
				
				
				$query2 	= "update lojas set foto = '$foto' where id = '$id_loja' ";
				$result2 	= mysql_query($query2);
				//echo $query2;
			}
		}
	}
	if($_FILES['logo']['name'] != "")
	{
		$nome_imagem 	= basename($_FILES['logo']['name']);
		$ext 			= explode('.', $nome_imagem);
		$extensao		= strtolower($ext[1]);
		
		if($extensao != "jpg")
		{
			alert('Apenas imagens de extens�o jpg s�o aceitas. A imagem n�o p�de ser cadastrada.');
			redirect("cadastro_loja.php");	
		}
		else
		{
			$path 		= "../images/lojas/logos/".$id_loja.".".$extensao;
			$logo		= $id_loja.".".$extensao;
			if (move_uploaded_file($_FILES['logo']['tmp_name'], $path )) 
			{
				//thumbit ($path,189,117,S);
				//thumbit ($path,320,240,N);
				thumbit ($path,117,117,N);
				thumbit ($path,117,117,S);
				
				$query2 = "update lojas set logomarca = '$logo' where id = '$id_loja' ";
				$result2 = mysql_query($query2);
				//echo $query2;
			}
		}
	}
	if(empty($nome) and empty($localizacao) )
	{
		alert('Preencha o nome e a localiza��o da sua Loja.');
		die(redirect('edita_loja.php?id_loja=$id_loja'));
	}
	
	$query_update = "update lojas set nome = '$nome', localizacao = '$localizacao', telefone = '$telefone', email = '$email', site = '$site', palavras_chave = '$palavras_chave' where id = $id_loja";
	$result_update = mysql_query($query_update)	;

	//gravando as subcategorias selecionadas para esta loja\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	$quant_subcategorias = count($subcategorias);
	if($quant_subcategorias > 0)
	{
		//apaga as antigas para gravar as novas
		if(mysql_query( "delete from subcategorias_lojas where id_loja = ".$id_loja." "))
		{
			for($i=0;$i<$quant_subcategorias;$i++)
				mysql_query("insert into subcategorias_lojas values(".$subcategorias[$i].",".$id_loja.")");
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	if($result_update)
	{
		alert('Altera��o realizada com sucesso');
		redirect("gerencia_lojas.php");	
	}
	else
	{
		alert('Erro ao alterar.');
		redirect("cadastro_loja.php?id_loja='".$id_loja."'");
	}
}
else
{
	$conteudo_tpl 		 = AbrePag(DIR_TEMPLATES.'edita_loja.html');
	$conteudo_tpl 		 = str_replace ('##ACTION##' , 'edita_loja.php' , $conteudo_tpl);
	
	$id_loja = $_GET['id_loja'];
	
	//******************** PEGANDO DADOS DA LOJA **********************\\
	$queryLoja 				 = "select * from lojas where id = $id_loja";
	$resultLoja 			 = mysql_query($queryLoja);
	$rowLoja 				 = mysql_fetch_array($resultLoja);
	//******************** EXIBINDO DADOS DA LOJA **********************\\
	$conteudo_tpl		 = str_replace ("##NOME##" , $rowLoja['nome'] , $conteudo_tpl);
	$conteudo_tpl		 = str_replace ('##LOCALIZACAO##' , $rowLoja['localizacao'] , $conteudo_tpl);
	$conteudo_tpl		 = str_replace ('##TELEFONE##' , $rowLoja['telefone'] , $conteudo_tpl);
	$conteudo_tpl		 = str_replace ('##EMAIL##'	, $rowLoja['email']	, $conteudo_tpl);
	$conteudo_tpl		 = str_replace ('##SITE##' , $rowLoja['site'] , $conteudo_tpl);
	$conteudo_tpl		 = str_replace ('##PALAVRAS_CHAVE##' , $rowLoja['palavras_chave'], $conteudo_tpl);
	//******************** EXIBINDO FOTO E LOGO DA LOJA *****************\\
	if($rowLoja['foto'] > 0){
		$foto = explode(".",$rowLoja['foto']);
		$foto_mini = $foto[0]."_mini.".$foto[1];
		$foto_mini = "<img src='../images/lojas/fachadas/$foto_mini' border='1' />";
		$conteudo_tpl 	 = str_replace("##FOTO##", $foto_mini , $conteudo_tpl);
		$acao ="<a href='exclui_loja.php?acao=excluir_foto&id_loja=$id_loja'>excluir foto</a>";
		$conteudo_tpl 	 = str_replace("##EXCLUIR_FOTO##", $acao , $conteudo_tpl);
	}else{
		$foto = "<img src='../images/sem_foto.jpg ' border='1' />";
		$conteudo_tpl 	 = str_replace("##FOTO##", $foto , $conteudo_tpl);
		$conteudo_tpl 	 = str_replace("##EXCLUIR_FOTO##", 'sem foto' , $conteudo_tpl);
	}
	if($rowLoja['logomarca'] > 0){
		$logo = explode(".",$rowLoja['logomarca']);
		$logo_mini = $logo[0]."_mini.".$logo[1];
		$logo_mini = "<img src='../images/lojas/logos/$logo_mini' border='1' />";
		$conteudo_tpl 	 = str_replace("##LOGO##", $logo_mini , $conteudo_tpl);
		$acao ="<a href='exclui_loja.php?acao=excluir_logo&id_loja=$id_loja'>excluir logo</a>";
		$conteudo_tpl 	 = str_replace("##EXCLUIR_LOGO##", $acao , $conteudo_tpl);
	}else{
		$logo = "<img src='../images/sem_foto.jpg' border='1' />";
		$conteudo_tpl 	 = str_replace("##LOGO##", $logo , $conteudo_tpl);
		$conteudo_tpl 	 = str_replace("##EXCLUIR_LOGO##", 'sem logo' , $conteudo_tpl);
	}
	
	//******************** PEGANDO AS CATEGORIAS E SUBCATEGORIAS DA LOJA **********************\\
	$queryCatSub = "select s.id as idSub, s.nome as nomeSub, s.id_categoria as father_id ,sl.*,c.nome as nomeCat from subcategorias s, subcategorias_lojas sl,categorias c where s.id = sl.id_subcategoria and s.id_categoria = c.id and sl.id_loja = '".$id_loja."' ";
	$resultCatSub = mysql_query($queryCatSub);
	
	$categorias_subcategorias = "";
	while($rowCatSub = mysql_fetch_array($resultCatSub))
		$categorias_subcategorias .= "<option value='".$rowCatSub['idSub']."'>".$rowCatSub['nomeCat']. " / ".$rowCatSub['nomeSub']."</option>";
		
	$conteudo_tpl 		 = str_replace("##CATEGORIAS_SUBCATEGORIAS##", $categorias_subcategorias , $conteudo_tpl);
	
	$submit_id 			 = "<input name= 'id_loja' type= 'hidden' value ='".$id_loja."' />";
	$conteudo_tpl		 = str_replace ("##HIDDEN##" , $submit_id , $conteudo_tpl);
				
	$FazCombo_categorias = FazComboCategoriasLoja('categorias', 'nome','categorias', '0', 'label');
	$conteudo_tpl 		 = str_replace("##CATEGORIAS##", $FazCombo_categorias,$conteudo_tpl);
	
	$subcategorias 		 = "<div id='subcategoria' style='display:inline'></div>";
	$conteudo_tpl 		 = str_replace("##SUBCATEGORIAS##", $subcategorias , $conteudo_tpl);
		
///////////////////////////////////////////////////////////////
	include("navegacao.php");
?>
	<div class="conteudo">
<?
	echo $conteudo_tpl;
?>
	</div> 
<?
	include("rodape.php");
	////////////////////////////////////////////////////////////////
}

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
?>