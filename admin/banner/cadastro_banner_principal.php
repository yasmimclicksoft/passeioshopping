<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
function array_remove_keys(&$array) {
$args = func_get_args();
if (count($args) == 1) return true;
array_shift($args);

$reIndex = false;
if (is_bool($args[count($args)-1])) $reIndex = array_pop($args);

foreach ($args as $value1) {
if (is_array($value1)) {
foreach ($value1 as $value2) array_remove_keys($array,$value2);
} else {
unset($array[$value1]);
}
}

if ($reIndex) $array = array_values($array);

return true;
}
///////////////////////////////////////
if(verificaPermissao("permissao_criar_post", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao)
{
	$submit = $_POST['submit'];
	
	if(isset($submit))
	{
		/*$data 	= $_POST['data'];*/
		$nome 			= $_POST['nome'];
		$ordem			= $_POST['ordem'];
		$data_entrada 	= entradaData($_POST['dataEntrada']);
		$data_saida		= entradaData($_POST['dataSaida']);
		$data = dataAtual();
		
		$ativa = $_POST['ativa'];
		
		$data_atual = date('Ymd');
		///retirando os "-" da data para depois ser feita a compara��o////////////////////////////////////////////////////////////
		$data_fim_numeral = str_replace("-", "",  $data_saida);
		
		if(($ativa == "on") and ($data_fim_numeral <= $data_atual))
		{ 
			alert("O banner n�o foi ativado pois sua data de sa�da j� foi expirada!");
			$ativa = 0;
		}
		elseif($ativa == "on")
		{
			$ativa = 1;
		}
		else
			$ativa = 0;

		//verificando se o arquivo importado � um swf, se n�o o usu�rio � redirecionado para p�gina de listagem sem cadastrar o arquivo
		if($_FILES['arquivo']['type'] != "application/x-shockwave-flash")
		{
			alert("Apenas arquivos com extensão swf são aceitas");
			redirect("gerencia_banner_principal.php");
		}
		else
		{
			$query	= "insert into banner_principal(nome,inicio,fim,flash,status,ordem) values('$nome','$data_entrada','$data_saida','','$ativa','$ordem')";
			
			$result = mysql_query($query);
			
			if($result)
			{
				$id_banner = mysql_insert_id();				
				
				if($_FILES['arquivo']['name'] != "")
				{
					$nome_banner 	= basename($_FILES['arquivo']['name']);
					$ext 			= explode('.', $nome_banner);
					$extensao		= strtolower($ext[1]);
					
					$path 		= "../../swf/".$id_banner."_".$ext[0].".".$ext[1];
					
					$arquivo	= $id_banner."_".$ext[0].".".$ext[1];
					
					if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $path )) 
					{
						$query2 ="update banner_principal set flash='$arquivo' where id = '$id_banner' ";
						$result2 = mysql_query($query2);
						//echo $query2;
					}
					
				}
				alert('Banner principal adicionado com sucesso.');
				redirect("gerencia_banner_principal.php");	
			 }//fim do result
			 else
			 {
				alert('Erro ao adicionar banner principal.');
				redirect("gerencia_banner_principal.php");	
			 }
		}
	
	}
	else ////////////////// abre a pagina \\\\\\\\\\\\\\\
	{
		$conteudo_tpl 	= AbrePag(DIR_TEMPLATES.'cadastro_banner_principal.html');
		$conteudo_tpl 	= str_replace ('##ACTION##', 'cadastro_banner_principal.php', $conteudo_tpl);
			
		//selecionando a ordem dispon�vel para exibi��o
		$query_ordem 	= "select * from banner_principal order by ordem";
		$result_ordem	= mysql_query($query_ordem);

		$query_count 	= "select count(id) as totalOpcoes from banner_principal order by ordem";
		$result_count 	= mysql_query($query_count);
		$totalOpcoes 	= mysql_result($result_count,0,'totalOpcoes');

		$limite = $totalOpcoes + 5;
		$i=1;
		$montaOrdem = array();
		$novoArray = array();
				
		for($j=1;$j<=$limite;$j++)
			$montaOrdem[] = $j;	
								
		while($row_ordem=mysql_fetch_array($result_ordem)){
			$key = array_search($row_ordem['ordem'], $montaOrdem); 
			if(isset($key))
				unset($montaOrdem[$key]);

			$i++;
		}

		foreach($montaOrdem as $key => $value)
			$novoArray[] = $value;
						
		$ordem_disponivel = "<select name='ordem'>";
		
		for($i=0;$i<count($novoArray);$i++)
			$ordem_disponivel .= "<option value='$novoArray[$i]'>$novoArray[$i]</option>";	
		
		$ordem_disponivel .= "</select>";
		
		$conteudo_tpl 	= str_replace ('##ORDENACAO##', $ordem_disponivel, $conteudo_tpl);
	
		$ativarHome = "<tr>
        	<td>Ativar banner na index do site:</td>
            <td><input type='checkbox' name='ativa' id='ativa' /></td>
        </tr>";

		$conteudo_tpl 	= str_replace ('##ATIVA_HOME##', $ativarHome, $conteudo_tpl);
		////////////////////////////////////////////////////////////////
		
		include("../navegacao.php");
		?>
		<div class="conteudo">
		<?
			echo $conteudo_tpl;
		?>
		</div> 
		<?
		
		////////////////////////////////////////////////////////////////
	}
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	require_once("../rodape.php");
	////////////////////////////////////////////////////////////////
?>