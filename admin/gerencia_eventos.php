<? include("topo.php"); 
////////////////////////////////////////
include("../includes/functions.php");
include("../fckeditor/fckeditor.php");

conexao();
////////////////////////////////////////
$id_user = $_GET['id_user'];
//echo $_SESSION['id_usuario'];
if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("index.php");
    die();
}
if($permissao){
	
	$altera_status = $_GET['altera_status'];
	
	if($altera_status == 1){
		$id_evento = $_GET['id_evento'];
		
		$query_status 	= "select status from eventos where id = $id_evento";
		$result_status	= mysql_query($query_status);
		$status 		= mysql_result($result_status, 0, 'status');
		if($status == 0)
		{ 
			$query_update	= "update eventos set status = 1 where id = $id_evento";
			$msg_status		= "Evento ativado na Home.";
		}
		else
		{
			$query_update	= "update eventos set status = 0 where id = $id_evento";
			$msg_status		= "Evento desativado na Home.";
		}
		$result_update	= mysql_query($query_update);
		
		if($result_update){
			alert($msg_status);
			redirect("gerencia_eventos.php");
            die();
			/*$query_update2 = "update novidades set status = 0 where id != $id_novidade";
			$result_update2= mysql_query($query_update2);*/
		}
	}

	///////// Preparando para paginacao \\\\\\\\\\\
	$order = "data_fim desc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros para paginacao
	$query = "SELECT count(id) as total from eventos";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from eventos order by $order limit $inicio,$limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_eventos.html');
	
	while ($row = mysql_fetch_array($result)) 
	{
		$id_evento		= $row['id'];
		$nome		= str_replace('"',"'",$row['nome']);
		$descricao 		= html_entity_decode($row['descricao']); 
		$localizacao	= $row['localizacao'];
		$data_inicio	= saidaData($row['data_inicio']);
		$data_fim		= saidaData($row['data_fim']);
		
		if($row['status'] == 1){
			$status = "<img src='../imagens_layout/tick.png ' border=0 alt='Evento ativo na home' />";
			$title 	= "Evento ativo na home";
		}else{
			$status = "<img src='../imagens_layout/slash.png ' border=0 alt='Evento inativo na home' />";
			$title 	= "Evento inativo na home";
		}
		
		/*
		$abrev =  substr($descricao , 0 , 45); 
		if(strlen($descricao) > 45) $abrev = $abrev. "...";*/
		$tamanho_max	= 80;		
		$abrev =  substr(strip_tags($descricao) , 0 , $tamanho_max); 
		if(strlen(strip_tags($descricao)) > $tamanho_max) $abrev = $abrev. "...";
		else $abrev = $descricao;
		
		$hora_inicio	= saidaHora($row['data_inicio']);
		$hora_fim		= saidaHora($row['data_fim']);
		
		$acao_editar	= "<a href='edita_evento.php?id_evento=$id_evento'><img src='../imagens_layout/btn_editar_p.jpg' border=0 /></a>";
		$acao_excluir	= "<a onclick=\"confirma('Deseja excluir esta o evento?', 'exclui_evento.php?id_evento=$id_evento');\" href='#'><img src='../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
			
		$campos 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt' nowrap>$nome</td>
								
								<td class='td_txt'>$localizacao</td>
								<td class='td_txt'>$data_inicio-$hora_inicio</td>
								<td class='td_txt'>$data_fim-$hora_fim</td>
								<td class='td_txt' align='center'>
									<a href='gerencia_eventos.php?altera_status=1&id_evento=$id_evento' border=0 title='$title'  >$status</a></td>
								<td class='td_txt' align=center nowrap >$acao_editar &nbsp; $acao_excluir</td>
							</tr> ";
		
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	///////////// paginacao \\\\\\\\\\\\\\\\\\
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_eventos.php?data_fim=$data_fim&order=data_fim desc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_eventos.php?data_fim=$data_fim&order=data_fim desc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_eventos.php?data_fim=$data_fim&order=data_fim desc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
	
		////////////////////////////////////////////////////////////////
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?

}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}
	include("rodape.php");
	////////////////////////////////////////////////////////////////
?>
