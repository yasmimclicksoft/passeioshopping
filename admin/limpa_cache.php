﻿<?
# Formata a data atual para o padrão brasileiro
@$now = @gmdate('D, d M Y H:i:s') . ' GMT';

# Expira a página ao mesmo tempo que compila e mostra
@header('Expires: ' . @$now);

# Indica que a última modificação foi no momento da compilação
@header('Last-Modified: ' . @$now);

# Indica ao navegador que não guarde a página
@header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1

# Indica ao navegador que não cheque se a página é a mesma que está no cache e que carregue a página
@header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1

# Indica ao navegador e ao proxy (se for o caso) que não use cache desta página
@header('Pragma: no-cache'); // HTTP/1.0

?>