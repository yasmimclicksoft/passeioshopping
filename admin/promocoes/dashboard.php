<? include("../topo.php"); 
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'dashboard.html');
	
	$submit		=	$_POST['submit'];
	$promocao	= 	$_POST['promocao'];
	
	if($submit)
	{
		
		if($promocao == 0)
		{
			$condicao = "";
		}
		else
		{
			$condicao = " and t.promocao_id = ".$promocao;
		}
		
		
	$query = "
select
	count(distinct(t.cliente_id)) as totalParticipantes,
	count(distinct(tnf.nota_fiscal)) as numeroNota,
	count(distinct(t.id)) as totalTrocas,
	(sum(distinct(tnf.valor_compra))*tnf.bonus)/100 as valorNotas,
	sum(tp.quantidade_produtos) as totalProdutos,
	floor((sum(tnf.valor_compra)*tnf.bonus)/100-count(tp.troca_id)) as valorProdutos,
	max(tnf.valor_compra) as valorMaior,
	floor(avg(distinct(tnf.valor_compra))*tnf.bonus)/100 as valorMedio,
	CEILING(avg(distinct(t.id))/count(distinct(t.cliente_id))) as trocasCliente,
	max(tp.quantidade_produtos) as quantidadeMaior
from 
	trocas t,
	trocas_notas_fiscais tnf,
	trocas_produto tp
where
	tp.troca_id = t.id
	and tnf.troca_id = t.id
	and tp.troca_id = tnf.troca_id
	".$condicao."";

	$result = mysql_query($query);
	
	$query_cupom = "
select
	distinct sum((pr.valor_cupom * b.porcentagem_bonus)/100) as valorCupom,
	count(tnf.bonus_id) as NumeroCupom,
	avg(distinct tnf.bonus_id) as mediaNCupons
from
	bonus b,
	promocoes pr,
	trocas t,
	trocas_notas_fiscais tnf
where 
	tnf.troca_id = t.id
	and tnf.bonus_id = b.id
	".$condicao."";
	
	$result_cupom = mysql_query($query_cupom);
	
	$dashboard = "<table class='tabela_listagem' cellpadding='0' cellspacing='0' border='0' width='100%'>";
	while($row = mysql_fetch_array($result) and $row_cupom = mysql_fetch_array($result_cupom))
	{
	
	$numeroCupons 	= $row_cupom['mediaNCupons'];
	$numeroClientes = $row['totalParticipantes'];
	$numeroTrocas	= $row['totalTrocas'];
	
	if($numeroCupons != 0)
	{
	$mediaCuponsC = $numeroCupons/$numeroClientes;
	
	$mediaCuponsT = $numeroCupons/$numeroTrocas;
	}
	$dashboard .= "<tr class='tr_txt txt_pag'>
         				<td class='td_txt'>Total de clientes participantes</td>
						<td class='td_txt' align='right'>".formatar_valor($row['totalParticipantes'])."</td>
         			</tr>
         			<tr class='tr_txt txt_pag'>
            			<td class='td_txt'>Total de trocas</td>
            			<td class='td_txt' align='right'>".formatar_valor($row['numeroNota'])."</td>
         			</tr>
         			<tr class='tr_txt txt_pag'>
            			<td class='td_txt'>N� de cupons</td>
            			<td class='td_txt' align='right'>".formatar_valor($row_cupom['NumeroCupom'])."</td>
         			</tr>
				   <tr class='tr_txt txt_pag '>
					  <td class='td_txt'>valor dos cupons</td>
					  <td class='td_txt' align='right'>R$ ".formatar_moeda($row_cupom['valorCupom'])."</td>
				   </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>N� de notas fiscais</td> 
					  <td class='td_txt' align='right'>".formatar_valor($row['numeroNota'])."</td> 
				   </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Valor das notas fiscais</td>
					  <td class='td_txt' align='right'>R$ ".formatar_moeda($row['valorNotas'])."</td>
				   </tr>
					<tr class='tr_txt txt_pag'>
					  <td class='td_txt'>N� total de produtos</td>
					  <td class='td_txt' align='right'>".formatar_valor($row['totalProdutos'])."</td>
				   </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Valor total de produtos</td>
					  <td class='td_txt' align='right'>R$ ".formatar_moeda($row['valorProdutos'])."</td>
				  </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Valor da Maior troca</td>
					  <td class='td_txt' align='right'>R$ ".formatar_moeda($row['valorMaior'])."</td>
				  </tr>
				  <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Quantidade da Maior troca</td>
					  <td class='td_txt' align='right'>".$row['quantidadeMaior']."</td>
				  </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Valor medio</td>
					  <td class='td_txt' align='right'>R$ ".formatar_moeda($row['valorMedio'])."</td>
				  </tr>
				  <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Quantidade m�dia trocas por clientes</td>
					  <td class='td_txt' align='right'>".$row['trocasCliente']."</td>
				  </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Quantidade m�dia de cupons por clientes</td>
					  <td class='td_txt' align='right'>".ceil($mediaCuponsC)."</td>
				  </tr>
				   <tr class='tr_txt txt_pag'>
					  <td class='td_txt'>Quantidade m�dia de cupons por troca</td>
					  <td class='td_txt' align='right'>".ceil($mediaCuponsT)."</td>
				  </tr>"; 
	}
	$dashboard .= "</table>";
	$conteudo_tpl 		= str_replace('##DASHBOARD##', $dashboard ,$conteudo_tpl);
	}
	else
	{
		$conteudo_tpl 	= str_replace('##DASHBOARD##', '' ,$conteudo_tpl);
	}
	////////////////////////////////////////////////////////////////
	
	include("../navegacao.php");
	
	include("busca_dashboard.php");
	
	?>
		<div class="conteudo">  	
	<?
			echo $conteudo_tpl;
	?>
        </div>
    <?
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>