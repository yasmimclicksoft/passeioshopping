<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_cadastrar_produto", $_SESSION['id_usuario']) and verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_produto", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\
	$order = "produto asc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros
	$query = "SELECT count(id) as total from produtos";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from produtos order by $order limit $inicio, $limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_produtos.html');
	
	$i = 0;
	while ($row = mysql_fetch_array($result)) 
	{
		$id_produto	= $row['id'];
		$produto	= $row['produto'];
		$cor 		= $row['cor'];
		$tipo		= $row['tipo'];
		$descricao	= $row['descricao'];
		$quantidade	= $row['quantidade'];
		$fornecedor = $row['fornecedor_id'];
				
		$query_grupo 	= "select nome from fornecedor where id = '$fornecedor'";
		$result_grupo 	= mysql_query($query_grupo);
		$row_grupo 		= @mysql_fetch_array($result_grupo);
		$nome_fornecedor			= $row_grupo['nome'];
		
					
		$acao_editar= "<a href='edita_produto.php?id_produto=$id_produto'><img src='../../imagens_layout/btn_editar_p.jpg' border=0 title='Editar Produto' /></a>";
		$acao_excluir	= "<a onclick=\"confirma('Deseja excluir este produto?', 'exclui_produto.php?id_produto=$id_produto');\" href='#'><img src='../../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
					
		$campos 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt'>$produto</td>
								<td class='td_txt'>$cor</td>
								<td class='td_txt'>$tipo</td>
								<td class='td_txt'>$descricao</td>
								<td class='td_txt'>$nome_fornecedor</td>
								
								<td class='td_txt' align='center' nowrap>$acao_editar &nbsp; $acao_excluir</td>
							</tr>";
		$i++;
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_produtos.php?produto=$produto&order=produto asc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_produtos.php?produto=$produto&order=produto asc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_produtos.php?produto=$produto&order=produto asc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
			
	////////////////////////////////////////////////////////////////
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("../navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?


}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
