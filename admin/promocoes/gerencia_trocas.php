<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_cadastrar_troca", $_SESSION['id_usuario']) )
{
	$permissao = true;
}
elseif(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\
	$order = "id desc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
	////////////////////////////////////////////////////////////////////////	
	
	$id_promocao = $_GET['id_promocao'];
	
	// pega o numero total de registros
	$query = "SELECT count(id) as total from trocas where promocao_id = $id_promocao";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from trocas where promocao_id = '$id_promocao' order by $order limit $inicio, $limite_pagina";
	//die();
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_trocas.html');
	
	
	
	//pego o nome da promocao
	$query_promocoes = "select nome_campanha from promocoes where id = $id_promocao";
	$result_promocoes= mysql_query($query_promocoes);
	$row_promocoes 	 = mysql_fetch_array($result_promocoes);
	$promocao 		 = $row_promocoes['nome_campanha'];
	
	$i = 0;
	
	while ($row = mysql_fetch_array($result)) 
	{
		$id_troca		= $row['id'];
		$id_produto		= $row['produto_id'];
		$id_cliente		= $row['cliente_id'];
		$id_user		= $row['usuario_id'];//usuario logado
		$data_troca		= saidaData($row['data_troca']);
		$quantidade		= $row['quantidade_produtos'];
			
		//pego o nome do produto
		$query_produtos = "select produto from produtos where id = $id_produto";
		$result_produtos= mysql_query($query_produtos);
		$row_produtos 	= mysql_fetch_array($result_produtos);
		$produto 		= $row_produtos['produto'];
		
		//pego o nome do cliente
		$query_clientes = "select nome_completo from clientes where id = $id_cliente";
		$result_clientes= mysql_query($query_clientes);
		$row_clientes 	= mysql_fetch_array($result_clientes);
		$cliente 		= $row_clientes['nome_completo'];
		
		//pego o nome do usuario que fez a troca
		$query_usuarios = "select nome from usuarios where id = $id_user";
		$result_usuarios= mysql_query($query_usuarios);
		$row_usuarios 	= mysql_fetch_array($result_usuarios);
		$usuario 		= $row_usuarios['nome'];
		
		//pegar os dados para exibi��o no detalhes
		$query_detalhes = "select p.produto as produto, tp.quantidade_produtos as quantidade from trocas_produto tp, produtos p where tp.produto_id=p.id and troca_id='$id_troca';";
		$result_detalhes = mysql_query($query_detalhes);
			
		$conteudo_tpl	 = str_replace ("##CLIENTE##" , $cliente , $conteudo_tpl);
		$conteudo_tpl	 = str_replace ("##DATATROCA##" , $data_troca , $conteudo_tpl);
		$conteudo_tpl	 = str_replace ("##PRODUTO##" , $produto , $conteudo_tpl);
		$conteudo_tpl	 = str_replace ("##QUANTIDADE##" , $quantidade , $conteudo_tpl);
		
		/*$acao_editar= "<a href='edita_troca.php?id_troca=$id_troca' ><img src='../../imagens_layout/btn_editar_p.jpg' border=0 title='Editar Troca' /></a>";*/
		while($row_detalhes = mysql_fetch_array($result_detalhes)){
		
		?>
			<div id="det<?=$i?>" style="display:none">
			<table class="tabela_listagem" cellpadding="0" cellspacing="0" >
				<tr class='tr_txt txt_pag'>
					<td>Nome:&nbsp;</td>
					<td class='tr_txt txt_pag'><?= $cliente ?></td>
				</tr>
				<tr class='tr_txt txt_pag'> 
					<td>Data:&nbsp;</td>
					<td class='tr_txt txt_pag'><?= $data_troca	?></td>
				</tr>
				<br />
				<tr class='tr_tit txt_tit'>
					<td class="td_tit">Produto</td>
					<td class="td_tit">Quantidade</td>
				</tr>
				<tr class='tr_txt txt_pag'>
					<td><?= $row_detalhes['produto'] ?></td>
					<td><?= $row_detalhes['quantidade']?></td>
				</tr>
			</table>
			</div>
		<?php
		}
		$acao_excluir	= "<a onclick=\"confirma('Deseja excluir esta troca?', 'exclui_troca.php?id_troca=$id_troca&id_promocao=$id_promocao');\" href='#'><img src='../../imagens_layout/btn_excluir_p.jpg' border=0 title='Excluir Troca' /></a>";
		$link_detalhe = "<a href='TB_inline?height=510&amp;width=600&amp;inlineId=det".$i."' class='thickbox txt_pag' title='Detalhes' ><img src='../../imagens_layout/info.gif' border='0'/></a>";
		$i++;
		
		
		$campos 		.= "<tr class='tr_txt txt_pag'>
								<td class='td_txt'>$cliente</td>
								<td class='td_txt'>$usuario</td>
								<td class='td_txt'>$data_troca</td>
								<td class='td_txt' align='center'>$link_detalhe</td>
								<td class='td_txt' align='center'> $acao_excluir</td>
							</tr>";
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	$conteudo_tpl 		= str_replace ('##PROMOCAO##', $promocao ,$conteudo_tpl);
	
	// id passado no link cadastra_troca.php
	$conteudo_tpl 		= str_replace ('##ID_PROMOCAO##', $id_promocao ,$conteudo_tpl);
	
	
	/////////////////////////////////////////////////////////////////////////////
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_trocas.php?id_promocao=$id_promocao&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_trocas.php?id_promocao=$id_promocao&p=".$i."\" class='texto_paginacao'> $i </a>";

					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_trocas.php?id_promocao=$id_promocao&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
			
	////////////////////////////////////////////////////////////////
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("../navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?


}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>

