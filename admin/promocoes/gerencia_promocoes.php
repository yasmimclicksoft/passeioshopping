<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_geral_promocao", $_SESSION['id_usuario']))
{
	$permissao = true;
}
elseif(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\
	$order = "nome_campanha asc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros
	$query = "SELECT count(id) as total from promocoes";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from promocoes order by $order limit $inicio, $limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_promocoes.html');
	
	$i = 0;
	while ($row = mysql_fetch_array($result)) 
	{
		$id_promocao	= $row['id'];
		$nome_campanha	= $row['nome_campanha'];
		$data_inicio	= saidaData($row['data_inicio']);
		$data_fim		= saidaData($row['data_fim']);
		$localizacao	= $row['localizacao'];
		$valorCupom		= $row['valor_cupom'];
		
		/*$acao_campanha	= "<a href='cria_campanha.php?id_promocao=$id_promocao' title='Criar Campanha para esta Promo��o'><img src='../../imagens_layout/btn_adicionar_p.jpg' border='0' title='Criar Campanha para esta Promo��o' /> Campanha</a>";*/
		
		/*$acao_trocar	= "<a href='cadastra_troca.php?id_promocao=$id_promocao' title='Cadastrar troca'><img src='../../imagens_layout/btn_adicionar_p.jpg' border='0' title='Cadastrar troca' /> Troca</a>";*/
		$acao_bonus	= "<a href='bonus.php?id_promocao=$id_promocao' title='Ver e vincular b�nus nesta Promo��o'><img src='../../imagens_layout/btn_adicionar_p.jpg' border='0' title='Ver e vincular produtos nesta Promo��o' /> B�nus</a>";
		
		$acao_produtos_promocao	= "<a href='produtos_promocao.php?id_promocao=$id_promocao' title='Ver e vincular produtos nesta Promo��o'><img src='../../imagens_layout/btn_adicionar_p.jpg' border='0' title='Ver e vincular produtos nesta Promo��o' /> Produtos/Promo��o</a>";
		
		$acao_gerenciar_trocas	= "<a href='gerencia_trocas.php?id_promocao=$id_promocao' title='Gerenciar trocas desta Promo��o'><img src='../../imagens_layout/btn_adicionar_p.jpg' border='0' title='Gerenciar trocas desta Promo��o' /> Trocas</a>";
		
		/*$acao_ver_campanha	= "<a href='lista_campanhas.php?id_promocao=$id_promocao' title='Informa��es da campanha desta Promo��o'><img src='../../imagens_layout/info.gif' border='0'  /></a>";*/
		
		$acao_editar= "<a href='edita_promocao.php?id_promocao=$id_promocao'><img src='../../imagens_layout/btn_editar_p.jpg' border=0 title='Editar Promo��o' /></a>";
		
		$acao_excluir	= "<a onclick=\"confirma('Deseja excluir esta promo��o?', 'exclui_promocao.php?id_promocao=$id_promocao');\" href='#'><img src='../../imagens_layout/btn_excluir_p.jpg' border=0 title='Excluir Promo��o' /></a>";
					
	$campos 		.= "<tr class='tr_txt txt_pag'>
							<td class='td_txt'>$nome_campanha</td>
							<td class='td_txt'>$data_inicio</td>
							<td class='td_txt'>$data_fim</td>
							<td class='td_txt'>$localizacao</td>
							<td class='td_txt'>R$ $valorCupom</td>
							<td class='td_txt' align='center' nowrap>$acao_bonus &nbsp;  $acao_produtos_promocao &nbsp;  $acao_gerenciar_trocas &nbsp; $acao_editar &nbsp; $acao_excluir</td>
						</tr>";
		$i++;
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_promocoes.php?promocao=$promocao&order=nome_campanha asc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_promocoes.php?promocao=$promocao&order=nome_campanha asc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_promocoes.php?promocao=$promocao&order=nome_campanha asc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
			
	////////////////////////////////////////////////////////////////
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	include("../navegacao.php");
	?>
		<div class="conteudo">
	<?			
			echo $conteudo_tpl;
	?>
	   		<div style="text-align:right;padding-right:20px;">
				<?  echo $paginacao; ?>
            </div>
        </div>
    <?


}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>

