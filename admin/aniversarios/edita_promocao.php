<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
include("../../fckeditor/fckeditor.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']) or verificaPermissao("permissao_geral_sac", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	$id = $_GET['id_promo'];

	//******************** PEGANDO DADOS DA PROMOCAO **********************\\
	$query_promo = "select *from promocoes_aniversario where id='$id'";
	$result_promo = mysql_query($query_promo);
	$row_promo = mysql_fetch_array($result_promo);
	//******************** EXIBINDO DADOS DA LOJA **********************\\
	$conteudo_tpl 		= AbrePag(DIR_TEMPLATES.'edita_promocao.html');
	
	$foto		=	$row_promo['foto_produto'];
	$logomarca	=	$row_promo['foto_logomarca'];
	
	$conteudo_tpl		 = str_replace ("##TITULO##" , $row_promo['titulo'], $conteudo_tpl);
	
	$FazCombo_Loja	 	= FazCombo('lojas', 'nome', 'lojas', $row_promo['loja_id'], 'label');
	$conteudo_tpl 		= str_replace("##LOJAS##", $FazCombo_Loja , $conteudo_tpl);
	
	$upload_html_form 	 = "<input class='label' name='foto' type='file' />";
	$conteudo_tpl 		 = str_replace ('##UPLOAD_FOTO##', $upload_html_form, $conteudo_tpl);
	
	$upload_html_form 	 = "<input class='label' name='logo' type='file' />";
	$conteudo_tpl 		 = str_replace ('##UPLOAD_LOGO##', $upload_html_form, $conteudo_tpl);
	
	// inserindo o editor de html
	$sBasePath = $_SERVER['PHP_SELF'];
	$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, '_samples' ) );
	$oFCKeditor = new FCKeditor('descricao'); 
	$oFCKeditor->BasePath = '../../fckeditor/';
	$oFCKeditor->Value = html_entity_decode($row_promo['descricao']); 
	$oFCKeditor->width = '100%';
	$oFCKeditor->Height = '200'; 
	$descricao = $oFCKeditor->Create();
	//fui em fckeditor e alterei a funcao Create() de echo para return
	$conteudo_tpl 		= str_replace ('##DESCRICAO##', $descricao , $conteudo_tpl);
	
	$submit	=	$_POST['submit'];
	$titulo = $_POST['titulo'];
	$loja_id = $_POST['lojas'];
	$descricao = htmlentities($_POST['descricao']);
	
	if(isset($submit))
	{
		if($_POST['foto'] != "")
		{
			@unlink("../../images/promocoes_aniversario/".$foto); 
		}
		
		if($_POST['logo'] != "")
		{
			@unlink("../../images/promocoes_aniversario/logo_promocao/".$logomarca); 
		}
		
		$id_promocao = $id;
		
		if($_FILES['foto']['name'] != "")
		{
			$nome_imagem 	= basename($_FILES['foto']['name']);
			$ext 			= explode('.', $nome_imagem);
			$extensao		= strtolower($ext[1]);
			
			/*if($extensao != "jpg")
			{
				alert('Apenas imagens de extens�o jpg s�o aceitas. A imagem n�o p�de ser cadastrada.');
				redirect("gerencia_lojas.php");	
			}*/
			/*else
			{*/
				$path 		= "../../images/promocoes_aniversario/".$id_promocao.".".$extensao;
				$foto		= $id_promocao.".".$extensao;
				if (move_uploaded_file($_FILES['foto']['tmp_name'], $path )) 
				{
					//thumbit ($path,156,117,S);
					//thumbit ($path,150,130,N);
					
					$query2 = "update promocoes_aniversario set foto_produto = '$foto' where id = '$id_promocao' ";
					$result2 = mysql_query($query2);
				}
			//}
		}
		
		if($_FILES['logo']['name'] != "")
			{
				$nome_imagem 	= basename($_FILES['logo']['name']);
				$ext 			= explode('.', $nome_imagem);
				$extensao		= strtolower($ext[1]);
			
					$path 		= "../../images/promocoes_aniversario/logo_promocao/".$id_promocao.".".$extensao;
					$logo		= $id_promocao.".".$extensao;
					
					if (move_uploaded_file($_FILES['logo']['tmp_name'], $path )) 
					{	
						
						$query2 = "update promocoes_aniversario set foto_logomarca = '$logo' where id = '$id_promocao' ";
						$result2 = mysql_query($query2);
						
					}
				
			}		
		
		$query = "update promocoes_aniversario set titulo = '$titulo', loja_id = '$loja_id', descricao = '$descricao' where id = '$id_promocao'";
		
		$result = mysql_query($query);
		
		if($result)
		{
			alert('Altera��o realizado com sucesso');
			redirect("gerencia_promocao.php");		
		}
		else
		{
			alert('Erro na altera��o');
			redirect('edita_promocao.php?id_promo=$id');
		}
	}

////////////////////////////////////////////////////////////////
	include("../navegacao.php");
?>
	<div class="conteudo">
<?
	echo $conteudo_tpl;
?>
	</div> 
<?
	include("../rodape.php");
	////////////////////////////////////////////////////////////////


}
else
{
	alert("Permissao Negada");
	redirect("index.php");
}

?>
