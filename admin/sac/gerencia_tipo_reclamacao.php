<? include("../topo.php"); 
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////
if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	///////// Preparando para paginacao \\\\\\\\\\\
	$order = "nome_reclamacao asc";
	
	$p = $_REQUEST['p'];
	if(!isset($p))
	{
		$p = 1;
	}
	
	//defino a qtde de linhas da paginacao
	$limite_pagina = 20;
		
	//pego o numero da pagina numero da pagina
	$p = $_GET['p'];
		
	//se o usuario alterar o valor de p na url, ele assumira que p = 1
	if(!isset($p))
	{
		$p = 1;
	}
	// defino o inicio
	$inicio = ($p-1) * $limite_pagina;
		
	// pega o numero total de registros
	$query = "SELECT count(id) as total from tipo_reclamacao";
	$result_total = mysql_query($query);
	$total_registros = mysql_result($result_total,0);
	
	$query = "select * from tipo_reclamacao order by $order limit $inicio,$limite_pagina";
	$result = mysql_query($query);
	
	$conteudo_tpl	= AbrePag(DIR_TEMPLATES.'gerencia_tipo_reclamacao.html');
	
	while ($row = mysql_fetch_array($result)) 
	{
		$nome 			= $row['nome_reclamacao'];
		$area_id		= $row['area_id'];
		$id				= $row['id'];
		
		$query_area 	= "select *from areas where id='$area_id'";
		$result_area	= mysql_query($query_area);
		$row_area		= mysql_fetch_array($result_area);
		$nome_area		= $row_area['nome'];
		
		$acao_editar	= "<a href='edita_tipo_reclamacao.php?id=$id'><img src='../../imagens_layout/btn_editar_p.jpg' border=0 /></a>";
		$acao_excluir	= "<a onclick=\"confirma('Tem certeza que deseja excluir a reclamação ".$nome."?', 'exclui_tipo_reclamacao.php?id=$id');\" href='#'><img src='../../imagens_layout/btn_excluir_p.jpg' border=0 /></a>";
			
		$campos 		.= "<tr class='tr_txt txt_pag' >
								<td class='td_txt'>$nome</td>
								<td class='td_txt'>$nome_area</td>
								<td class='td_txt' align=center nowrap='nowrap' width='10%'>$acao_editar &nbsp;&nbsp; $acao_excluir</td></tr> ";
		
	}
	$conteudo_tpl 		= str_replace ('##CAMPOS##', $campos ,$conteudo_tpl);
	
	$max = $limite_pagina;
	// Calculando pagina anterior
	$menos = $p - 1;
	// Calculando pagina posterior
	$mais = $p + 1;
	$pgs = ceil($total_registros / $max);
			
	if( $pgs > 1 )
	{
		if($menos > 0)
			$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_tipo_reclamacao.php?nome=$nome&order=nome asc&p=".$menos."\" class='texto_paginacao'>Anterior </a>";
			
				if ( ($p-9) < 1 )
					$anterior = 1;
				else
					$anterior = $p-9;
			
				if ( ($p+9) > $pgs )
					$posterior = $pgs;
				else
					$posterior = $p + 9;
			
				for($i=$anterior;$i <= $posterior;$i++)
					if($i != $p)
						$paginacao .= "<a class=\"txt_pag_branco\" href=\"gerencia_tipo_reclamacao.php?nome=$nome&order=nome asc&p=".$i."\" class='texto_paginacao'> $i </a>";
					else
						$paginacao .= "<span class=\"txt_pag_azul\">".$i."</span>";
					if($mais <= $pgs)
						$paginacao .= "<a class=\"txt_pag\" href=\"gerencia_tipo_reclamacao.php?nome=$nome&order=nome asc&p=".$mais."\" class='texto_paginacao'> Proxima</a>";
	}// fim if ( $pgs > 1 )
			
	////////////////////////////////////////////////////////////////
	include("../navegacao.php");
	
	$conteudo_tpl = str_replace("##PAGINACAO_TOPO##", $paginacao, $conteudo_tpl );
	
	?>
		<div  class="conteudo">
        
	<?
			echo $conteudo_tpl;
			//echo $paginacao;
	?>
	   		<div style="text-align:right;padding-right:20px;"><? echo $paginacao; ?></div>
        </div>
	<?
	
}
else
{
	alert("Permissao Negada");
	redirect("../index.php");
}
	include("../rodape.php");
	////////////////////////////////////////////////////////////////
?>
