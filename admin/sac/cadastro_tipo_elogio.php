<? include("../topo.php");
////////////////////////////////////////
include("../../includes/functions.php");
conexao();
////////////////////////////////////////

if(verificaPermissao("permissao_geral_admin", $_SESSION['id_usuario']))
{
	$permissao = true;
}
else
{
	$permissao = false;
	alert("Permissao Negada");
	redirect("../index.php");
}
if($permissao){

	$submit = $_POST['submit'];

	if (isset($submit))
	{
		$nome = $_POST['nome']; 
		$area = $_POST['areas'];
		if($nome != "")
		{
			$query = "select id from tipo_elogio where nome_elogio = '$nome'";
			$result = mysql_query($query);
			$num_rows = mysql_num_rows($result);
			
			if($num_rows > 0)
			{
				alert('Já existe um elogio com este nome.');
				redirect("cadastro_tipo_elogio.php");	
			}
			else
			{
				$query_insert = "insert into tipo_elogio(nome_elogio,area_id) values('$nome','$area')";
				$result_insert = mysql_query($query_insert);
				
				if($result_insert)
				{
					alert('Elogio criado com sucesso.');	
					redirect("gerencia_tipo_elogio.php");	
				}
				else
				{
					alert('Erro ao cadastrar tipo de elogio.');	
					redirect("cadastro_tipo_elogio.php");
				}
			}
		}
		else
		{
			alert('Preencha o nome do elogio.');	
			redirect("cadastro_tipo_elogio.php");
		}
	}
	else
	{
		$conteudo_tpl = AbrePag(DIR_TEMPLATES.'cadastro_tipo_elogio.html');
		$conteudo_tpl = str_replace ('##ACTION##', 'cadastro_tipo_elogio.php', $conteudo_tpl);
		
		$FazCombo_categorias = FazCombo('areas', 'nome', 'areas', '0', 'label');
		$conteudo_tpl 		 = str_replace("##AREAS##", $FazCombo_categorias,$conteudo_tpl);
	
	////////////////////////////////////////////////////////////////
		
		include("../navegacao.php");
	?>
		<div class="conteudo">
	<?
		echo $conteudo_tpl;
	?>
		</div> 
	<?
	
		include("../rodape.php");
		////////////////////////////////////////////////////////////////
	}
}
?>