// jCarousel Plugin 
$ ( '#carousel' ) .jcarousel ({
    vertical: verdade , // orientação do carrossel, neste caso, usamos verticais 
    de rolagem: 1 , // o número de itens para rolar pela 
    auto: 2 , // o intervalo de rolagem 
    envoltório: 'última' , // envoltório em último item e saltar de volta para o início 
    initCallback: mycarousel_initCallback    // vamos usar isso para melhorar ainda mais o comportamento desse carrossel
});

$ ( Documento ) .ready ( função () {
        
    // jCarousel Plugin 
    $ ( '#carousel' ) .jcarousel ({
        vertical: verdade ,  // exibição vertical do carrossel 
        de rolagem: 1 ,  // auto rolagem 
        automática: 2 ,  // a velocidade da rolagem 
        envoltório: 'última' ,   // voltar ao topo quando alcance último item 
        initCallback: mycarousel_initCallback    // extra chamado função de volta
    });

    // Frente Carousel página - Configuração inicial 
    // definir tudo o item a completa opacidade 
    $ ( 'div # slideshow-carrossel um img' ) CSS ({ 'opacidade' : '0,5' });
    
    // reajustar o primeiro item a 50% de opacidade 
    $ ( 'div # slideshow-carrossel um img: first' ) CSS ({ 'opacidade' : '1.0' });
    
    // anexa a seta para o primeiro item 
    $ ( 'div # slideshow-carrossel li a: first' ) .append ( '' )

 
    // Adicionar foco e clique evento para cada um dos itens no carrossel 
    $ ( 'div # slideshow-carrossel li a' ) .hover (
         função () {
            
            // verifique se o item não estiver selecionado 
            , se (! $ ( este ) .Tem ( 'intervalo' ) .length) {
                 // reiniciar toda a opacidade do item para 50% 
                $ ( 'div # slideshow-carrossel li um img ' ) .Stop ( verdadeiro , verdadeiro ) .css ({ ' opacidade ' : ' 0,5 ' });
                
                // adust o item selecionado atual para completa opacidade 
                $ ( este ) .Stop ( verdadeiros , verdadeiros ) .children ( 'img' ) CSS ({ 'opacidade' : '1.0' });
            }       
        },
        função () {
                
            // on mouse para fora, redefinir todo o item de volta para 50% de opacidade 
            $ ( 'div li # slideshow-carrossel um img' ) .Stop ( verdadeiro , verdadeiro ) .css ({ 'opacidade' : '0,5' });
            
            // reativar o item selecionado por um loop por eles e olhar para o 
            // que tem a extensão de seta 
            $ ( 'div # slideshow-carrossel li a' ) .each ( função () {
                 // Encontrado a extensão e redefinir a opacidade de volta a opacidade total 
                , se ($ ( este ) .Tem ( 'intervalo' ) .length) $ ( este ) .children ( 'img' ) CSS ({ 'opacidade' : '1.0' });

            });
                
        }
    ) .click ( função () {

        // remover o span.arrow 
        $ ( 'span.arrow' ) .remove ();
        
        // anexa-lo para o atual item de         
        $ ( este ) .append ( '' );
        
        // remove a classe ativa do slideshow principal 
        $ ( 'div # slideshow-main li' ) .removeClass ( 'ativo' );
        
        // exibe a imagem principal, acrescentando classe ativa a ele.         
        $ ( 'li # slideshow-main div.' + $ ( este ) .attr ( 'rel' )) addClass (. 'ativa' ); 
            
        retornar  false ;
    });


});


// Carousel Tweaking 
função  mycarousel_initCallback ( carrossel ) {
    
    // Pausa deslocamento automático se o usuário se move com o cursor sobre o clipe. 
    // Retomar o contrário 
    carousel.clip.hover ( função () {
        carousel.stopAuto ();
    }, A função () {
        carousel.startAuto ();
    });
}
    